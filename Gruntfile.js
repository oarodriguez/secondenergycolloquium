module.exports = function(grunt) {

    'use strict';

    /* Configuration */
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch : {
            less : {
                files : 'ngapp/app/less/*.less',
                tasks : ['less:ngappDevelopment'],
                options : {
                    interrupt: true
                }
            },
            lessStatic : {
                files : 'static/less/*.less',
                tasks : ['less:staticSiteDevelopment'],
                options : {
                    interrupt: true
                }
            },
            jinjaStatic: {
                files: 'static/templates/*.html',
                tasks: ['jinja:staticSite'],
                options : {
                    interrupt: true
                }
            },
            concat: {
                files: [
                    'ngapp/app/js/config-dev.js',
                    'ngapp/app/js/app.js',
                    'ngapp/app/js/controllers.js',
                    'ngapp/app/js/extracontrollers.js',
                    'ngapp/app/js/directives.js',
                    'ngapp/app/js/filters.js',
                    'ngapp/app/js/plugins.js',
                    'ngapp/app/js/services.js'
                ],
                tasks: ['concat:ngappDevelopment']
            },
            concatStatic : {
                files: ['static/js/plugins.js'],
                tasks: ['concat:staticSiteDevelopment']
            }
        },
        less : {
            ngappDevelopment: {
                options: {
                    paths: ['ngapp/app/less', 'ngapp/app/less/vendor']
                },
                files : {
                    'ngapp/app/css/app.min.css': 'ngapp/app/less/app.less'
                }
            },
            ngappProduction : {
                options : {
                    paths : ['ngapp/app/less', 'ngapp/app/less/vendor'],
                    yuicompress : true
                },
                files : {
                    'ngapp/app/css/app.min.css': 'ngapp/app/less/app.less'
                }
            },
            staticSiteDevelopment: {
                options: {
                    paths: ['static/bower_components', 'static/less', 'static/less/vendor']
                },
                files : {
                    'static/css/site.min.css': 'static/less/site.less'
                }
            },
            staticSiteProduction: {
                options: {
                    paths: ['static/less', 'static/less/vendor'],
                    yuicompress : true
                },
                files : {
                    'static/css/site.min.css': 'static/less/site.less'
                }
            }
        },
        concat : {
            options: {
                separator: ';'
            },
            ngappDevelopment : {
                src: [
                    'ngapp/app/js/config-dev.js',
                    'ngapp/app/js/app.js',
                    'ngapp/app/js/controllers.js',
                    'ngapp/app/js/extracontrollers.js',
                    'ngapp/app/js/directives.js',
                    'ngapp/app/js/filters.js',
                    'ngapp/app/js/plugins.js',
                    'ngapp/app/js/services.js'
                ],
                dest: 'ngapp/app/js/regapp.min.js'
                // dest: 'tmp/ngapp/js/regapp-devel.js'
            },
            ngappProduction : {
                src: [
                    'ngapp/app/js/config.js',
                    'ngapp/app/js/app.js',
                    'ngapp/app/js/controllers.js',
                    'ngapp/app/js/extracontrollers.js',
                    'ngapp/app/js/directives.js',
                    'ngapp/app/js/filters.js',
                    'ngapp/app/js/plugins.js',
                    'ngapp/app/js/services.js'
                ],
                dest: 'tmp/ngapp/js/regapp.js'
            },
            staticSiteDevelopment: {
                src: [
                    'static/bower_components/bootstrap/dist/js/bootstrap.min.js',
                    'static/js/plugins.js'
                ],
                dest: 'static/js/main.js'
            }
        },
        uglify : {
            ngappDevelopment : {
                files : {
                    'ngapp/app/js/regapp.min.js': ['tmp/ngapp/js/regapp-devel.js']
                }
            },
            ngappProduction : {
                files : {
                    'build/ngapp/js/regapp.min.js': ['tmp/ngapp/js/regapp.js']
                }
            },
            staticSiteProduction : {
                files : {
                    'static/js/main.min.js': ['static/js/main.js']
                }
            }
        },
        jinja : {
            staticSite: {
                options: {
                    templateDirs: ['static/templates']
                },
                files: [{
                    expand: true,
                    cwd: 'static/templates',
                    src: ['**/!(_)*.html'],
                    dest: 'static/'
                }]
            }
        },
        copy: {
            ngappProduction: {
                files: [{
                    expand: true,
                    flatten: false,
                    cwd: 'ngapp/app',
                    src: [
                        'web.config',
                        'index.html',
                        'favicon.ico',
                        'logo-icon.png',
                        'css/app.min.css',
                        'css/fonts/!(*.db)',
                        // 'js/regapp.min.js',
                        'js/vendor/!(*.db)',
                        'img/{,*/}!(*.db)',
                        'partials/!(*.db)'
                    ],
                    dest: 'build/ngapp',
                    filter: 'isFile'
                }]
            },
            staticSiteProduction: {
                files: [{
                    expand: true,
                    flatten: false,
                    cwd: 'static/',
                    src: [
                        '*.html',
                        'favicon.ico',
                        'web.config',
                        'logo-icon.png',
                        'css/site.min.css',
                        'css/fonts/*',
                        'files/{,*/}!(*.db)',
                        'js/!(*.db)',
                        'js/vendor/*',
                        'img/{,*/}!(*.db)'
                    ],
                    dest: 'build/static',
                    filter: 'isFile'
                }]
            }
        }
    });


    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-jinja');

    grunt.registerTask('default', [
        'development'
    ]);

    grunt.registerTask('ngappDevelopment', [
        'less:ngappDevelopment',
        'concat:ngappDevelopment'
    ]);

    grunt.registerTask('ngappProduction', [
        'less:ngappProduction',
        'concat:ngappProduction',
        'uglify:ngappProduction'
    ]);

    grunt.registerTask('ngappBuild', [
        'ngappProduction',
        'copy:ngappProduction'
    ]);

    grunt.registerTask('staticSiteDevelopment', [
        'less:staticSiteDevelopment',
        'concat:staticSiteDevelopment',
        'jinja:staticSite'
    ]);

    grunt.registerTask('staticSiteProduction', [
        'less:staticSiteDevelopment',
        'concat:staticSiteDevelopment',
        'uglify:staticSiteProduction',
        'jinja:staticSite'
    ]);

    grunt.registerTask('staticBuild', [
        'staticSiteProduction',
        'copy:staticSiteProduction'
    ]);

    grunt.registerTask('buildAll', [
        'ngappBuild',
        'staticBuild'
    ]);


};
