# -*- encoding: utf-8 -*-

import os
import datetime


SECRET_KEY = '7ASgzcDWdZglL7uUhfl40ZOKTiHyK6nBBrBpZCb4AmBvdE0glC'
APPLICATION_ROOT = os.path.dirname(os.path.realpath(__file__))
SQLALCHEMY_DATABASE_URI = (
    'sqlite:///' + os.path.join(APPLICATION_ROOT, 'dbs', 'appdb.db'))
TRAP_HTTP_EXCEPTIONS = True
REST_API_PREFIX = '/api'
USE_X_SENDFILE = True
DEBUG = True


# Use Flask-Mail or Flask-Sendmail
USE_FLASK_MAIL = True

MAIL_SERVER = '127.0.0.1'
MAIL_USERNAME = ''
MAIL_PASSWORD = ''
MAIL_DEFAULT_SENDER = 'Coloquio de Energía <coloquio@champagn.fciencias.unam.mx>'

# Sendmail variables change
DEFAULT_MAIL_SENDER = MAIL_DEFAULT_SENDER

del datetime
del os
