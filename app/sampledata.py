# -*- encoding: utf-8 -*-
"""
    app.sampleadata
    ~~~~~~~~~~~~~~~

    Contains sample data to fill the database and test the application.

    :copyright: (c) 2013 by O. A. Rodríguez.
    :license: BSD-3, see LICENSE for more details.
"""
from werkzeug.datastructures import ImmutableList
from base64 import b64encode

USERS = ImmutableList([
    {
        # password: gatogarabato plain
        'email': 'admin@hotmail.com',
        'password': 'gatogarabato',
        'name': 'Usuario Administrador',
        'role': 'ADMINISTRATOR',
        'active': True
    },
    {
        'email': 'test_user1@hotmail.com',
        'password': 'testuser1abc',
        'name': 'Usuario 2',
        'active': True
    },
    {
        'email': 'test_user2@hotmail.com',
        'password': 'testuser2abc',
        'name': 'Usuario 3',
        'active': True
    }
])

AUTHORS = ImmutableList([
    {
        'firstName': u'Omar Abel',
        'lastName': u'Rodríguez López',
        'email': 'gato@gato.com',
        'occupation': u'Estudiante',
        'affiliation': u'Instituto de Física, UNAM',
        'affiliationAddress': u'Coyoacán',
        'phone': '(55)55555555'
    },
    {
        'firstName': u'Pablo de la Mora',
        'lastName': u'Palomar y Askinasy',
        'email': 'pablo@ciencias.unam.com',
        'occupation': u'Investigador Asociado',
        'affiliation': u'Facultad de Ciencias, UNAM',
        'affiliationAddress': u'Coyoacán',
        'phone': '(55)12345678'
    },
    {
        'firstName': u'María Sabina',
        'lastName': u'Ruiz Chavarría',
        'email': 'pablo@ciencias.unam.com',
        'occupation': u'Investigador Asociado',
        'affiliation': u'Facultad de Ciencias, UNAM',
        'affiliationAddress': u'Coyoacán',
        'phone': '(55)12345678'
    }
])

WORKS = ImmutableList([
    {
        'title': u'El efecto de la presión en las tormentas',

        'body': u"""Lorem ipsum dolor sit amet, consectetur adipiscing
        elit. Vivamus a scelerisque erat. Donec placerat ipsum libero, ac
        euismod lectus consequat at. Integer posuere nulla quis enim
        dapibus gravida. Duis eget volutpat elit, sed placerat purus.
        Maecenas elementum est ut pharetra gravida. Maecenas tempor
        ante quis dolor facilisis, non feugiat mi tristique. Praesent
        ornare scelerisque enim, et porta quam elementum ac. Nullam
        dapibus enim aliquam, pretium eros eget, blandit eros. Maecenas
        egestas sapien consequat ipsum posuere, eget convallis massa
        bibendum. Curabitur rutrum sapien sit amet mauris rhoncus
        suscipit. Ut velit lorem, laoreet et interdum quis, commodo ut
        quam. Curabitur luctus ante quis ante semper, nec vulputate
        elit fermentum. Integer ultrices nisl dolor, eget porttitor
        felis egestas sagittis.

        Praesent condimentum fermentum mauris, et molestie ipsum
        sollicitudin eget. Donec nec sem molestie, pretium neque nec,
        faucibus dui. Nunc in ligula vel nisi ornare porta at id ligula.
        Suspendisse tempus, elit faucibus molestie congue, purus felis
        lacinia neque, dictum suscipit eros urna vitae nibh. Maecenas
        euismod diam sed augue convallis, ac commodo nulla molestie.
        Praesent condimentum molestie turpis. In hendrerit nibh erat,
        in lobortis tellus consequat at. Cras accumsan mi eu auctor
        venenatis.

        Nam posuere molestie sagittis. Sed pellentesque justo a
        ullamcorper interdum. Suspendisse potenti. Nam in erat eu augue
        gravida eleifend eu tempus magna. Donec quam magna, rhoncus vel
        commodo et, lobortis eu lacus. Duis auctor lacus erat, cursus
        lobortis diam dictum in. Fusce risus ipsum, accumsan quis odio eu,
        dapibus varius augue. Nunc vel eleifend nibh. Proin porta dolor
        at tortor interdum tempor.""",

        'eventMode': 'POSTER'
    },
    {
        'title': u'La expansión del universo',

        'body': u"""Lorem ipsum dolor sit amet, consectetur adipiscing
        elit. Vivamus a scelerisque erat. Donec placerat ipsum libero, ac
        euismod lectus consequat at. Integer posuere nulla quis enim
        dapibus gravida. Duis eget volutpat elit, sed placerat purus.
        Maecenas elementum est ut pharetra gravida. Maecenas tempor
        ante quis dolor facilisis, non feugiat mi tristique. Praesent
        ornare scelerisque enim, et porta quam elementum ac. Nullam
        dapibus enim aliquam, pretium eros eget, blandit eros. Maecenas
        egestas sapien consequat ipsum posuere, eget convallis massa
        bibendum. Curabitur rutrum sapien sit amet mauris rhoncus
        suscipit. Ut velit lorem, laoreet et interdum quis, commodo ut
        quam. Curabitur luctus ante quis ante semper, nec vulputate
        elit fermentum. Integer ultrices nisl dolor, eget porttitor
        felis egestas sagittis.

        Praesent condimentum fermentum mauris, et molestie ipsum
        sollicitudin eget. Donec nec sem molestie, pretium neque nec,
        faucibus dui. Nunc in ligula vel nisi ornare porta at id ligula.
        Suspendisse tempus, elit faucibus molestie congue, purus felis
        lacinia neque, dictum suscipit eros urna vitae nibh. Maecenas
        euismod diam sed augue convallis, ac commodo nulla molestie.
        Praesent condimentum molestie turpis. In hendrerit nibh erat,
        in lobortis tellus consequat at. Cras accumsan mi eu auctor
        venenatis.

        Nam posuere molestie sagittis. Sed pellentesque justo a
        ullamcorper interdum. Suspendisse potenti. Nam in erat eu augue
        gravida eleifend eu tempus magna. Donec quam magna, rhoncus vel
        commodo et, lobortis eu lacus. Duis auctor lacus erat, cursus
        lobortis diam dictum in. Fusce risus ipsum, accumsan quis odio eu,
        dapibus varius augue. Nunc vel eleifend nibh. Proin porta dolor
        at tortor interdum tempor.""",

        'eventMode': 'SIMULTANEOUS'
    },
    {
        'title': u'Contaminación en el Valle de México',

        'body': u"""Lorem ipsum dolor sit amet, consectetur adipiscing
        elit. Vivamus a scelerisque erat. Donec placerat ipsum libero, ac
        euismod lectus consequat at. Integer posuere nulla quis enim
        dapibus gravida. Duis eget volutpat elit, sed placerat purus.
        Maecenas elementum est ut pharetra gravida. Maecenas tempor
        ante quis dolor facilisis, non feugiat mi tristique. Praesent
        ornare scelerisque enim, et porta quam elementum ac. Nullam
        dapibus enim aliquam, pretium eros eget, blandit eros. Maecenas
        egestas sapien consequat ipsum posuere, eget convallis massa
        bibendum. Curabitur rutrum sapien sit amet mauris rhoncus
        suscipit. Ut velit lorem, laoreet et interdum quis, commodo ut
        quam. Curabitur luctus ante quis ante semper, nec vulputate
        elit fermentum. Integer ultrices nisl dolor, eget porttitor
        felis egestas sagittis.

        Praesent condimentum fermentum mauris, et molestie ipsum
        sollicitudin eget. Donec nec sem molestie, pretium neque nec,
        faucibus dui. Nunc in ligula vel nisi ornare porta at id ligula.
        Suspendisse tempus, elit faucibus molestie congue, purus felis
        lacinia neque, dictum suscipit eros urna vitae nibh. Maecenas
        euismod diam sed augue convallis, ac commodo nulla molestie.
        Praesent condimentum molestie turpis. In hendrerit nibh erat,
        in lobortis tellus consequat at. Cras accumsan mi eu auctor
        venenatis.

        Nam posuere molestie sagittis. Sed pellentesque justo a
        ullamcorper interdum. Suspendisse potenti. Nam in erat eu augue
        gravida eleifend eu tempus magna. Donec quam magna, rhoncus vel
        commodo et, lobortis eu lacus. Duis auctor lacus erat, cursus
        lobortis diam dictum in. Fusce risus ipsum, accumsan quis odio eu,
        dapibus varius augue. Nunc vel eleifend nibh. Proin porta dolor
        at tortor interdum tempor.""",

        'eventMode': 'POSTER'
    }
])
