# -*- encoding: utf-8 -*-
"""
    app
    ~~~~

    Aplicación RESTful para administrar usuarios y trabajos
    para el Segundo Coloquio de Energía, Facultad de Ciencias,
    UNAM

    :copyright: (c) 2015 by O. A. Rodríguez
    ;license: BSD-3, see LICENSE for more details.
"""

from flask import Flask, json, render_template_string, abort, url_for, session
from flask.globals import request, g
from flask.helpers import send_from_directory, make_response, send_file
from flask.templating import render_template
from flask.wrappers import Response
from flask_restful import Resource, Api
from flask_restful.reqparse import RequestParser
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from formencode import validators, compound
from formencode.api import FancyValidator, Invalid
from formencode.schema import Schema
from formencode.validators import FormValidator
from functools import wraps
from smtplib import SMTPException
from sqlalchemy.orm import backref
from sqlalchemy.orm.exc import FlushError
from werkzeug.datastructures import ImmutableList
from werkzeug.security import generate_password_hash, check_password_hash
from xlsxwriter.workbook import Workbook
import base64
import datetime
import flask_mail as FlaskMail
import flask_sendmail as Sendmail
import hashlib
import hmac
import os
import pytz
import re


ROLES = {'ADMINISTRATOR': 1, 'REGISTERED': 2}
EVENT_ROLES = {'VISITOR': 1, 'EXPOSITOR': 2}
WORK_MODE = {'ORAL': 1, 'POSTER': 2, 'PROTOTYPE': 3}
VAL_MSGS = 'validationMessages'
AUTH_TOKEN = 'authorizationToken'
MAX_BODY_WORDS = 300


AUTHOR_TAGS = ImmutableList([
    u'Computación',
    u'Ciencia',
    u'Análisis Numérico',
    u'Estado Sólido',
    u'Cómputo'
])

WORK_TAGS = ImmutableList([
    u'Energía Solar',
    u'Energía Eólica',
    u'Energía Maremotriz',
    u'Energía Geotérmica',
    u'Biomasa',
    u'Energías Renovables en General',
    u'Experimentos, Prototipos y aplicaciones',
    u'Impacto Ambiental',
    u'Política Energética',
    u'Crisis Energética',
    u'Uso actual y futuro de la energía en México y el Mundo',
    u'Otra'
])


# Status codes
OK = 200
CREATED = 201
BAD_REQUEST = 400
NOT_ALLOWED = 405
NOT_FOUND = 404
INTERNAL_SERVER_ERROR = 500
FORBIDDEN = 403


USER_NOT_FOUND_ERROR = {
    'ERROR':    'The requested User was not found on the server',
    'status':   NOT_FOUND
}

AUTHOR_NOT_FOUND_ERROR = {
    'ERROR':    'The requested Author was not found on the server',
    'status':   NOT_FOUND
}

WORK_NOT_FOUND_ERROR = {
    'ERROR':    'The requested Work was not found on the server',
    'status':   NOT_FOUND
}

LOGIN_BAD_REQUEST = {
    'ERROR':    'User credentials must be passed as a JSON object',
    'status':   BAD_REQUEST
}

RESOURCE_FORBIDDEN = {
    'ERROR':    'User has no access to the requested resource',
    'status':   FORBIDDEN
}

LOGIN_SUCCESS = {
    'message':   'Login success',
    'status':   OK
}

LOGOUT_SUCCESS = {
    'message':   'User logged out',
    'status':   OK
}

REGISTRATION_SUCCESS = {
    'message':  'User registered.',
    'status':   OK
}

DELETE_SUCCESS = {
    'message':  'The resource has been deleted sucessfully.',
    'status':   OK
}


app = Flask('app', static_url_path='/static')
app.config.from_object('app.config')
app.config.from_pyfile('configregsys.py', silent=True)
if app.config['USE_FLASK_MAIL'] is True:
    Mail = FlaskMail.Mail
    Message = FlaskMail.Message
else:
    Mail = Sendmail.Mail
    Message = Sendmail.Message

api = Api(app, prefix=app.config['REST_API_PREFIX'], catch_all_404s=False)
db = SQLAlchemy(app)
mail = Mail(app)

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

# Some shorcuts
Model = db.Model
Column = db.Column
Integer = db.Integer
Text = db.Text
String = db.String
Unicode = db.Unicode
ForeignKey = db.ForeignKey
DateTime = db.DateTime
Boolean = db.Boolean
Table = db.Table
dbsession = db.session


####################################################
# #             SQLAlchemy models
####################################################
class User(Model):
    """Represent a user allowed to register new works."""
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    email = Column(String(255), unique=True)
    password = Column(String(1024))
    name = Column(Unicode(255))
    active = Column(Boolean)

    role = Column(Integer)
    last_access = Column(DateTime)
    works = db.relationship('Work', order_by='Work.id', backref='user',
        cascade="all, delete, delete-orphan")
    authors = db.relationship('Author', order_by='Author.id', backref='user',
        cascade="all, delete, delete-orphan")
    tokens = db.relationship('AuthenticationToken',
         cascade="all, delete, delete-orphan",
         order_by='AuthenticationToken.id', backref='user')

    def __init__(self, email, password, name=None, active=True, role=None,
                 event_role=None):
        self.email = email
        self.password = password
        self.name = name
        #Auto activated users, by now...
        self.active = active
        self.last_access = datetime.datetime.now()
        self.role = ROLES.get(role, ROLES['REGISTERED'])

    def __repr__(self):
        return '<User %r>' % self.email


# : The table to store the many to many relationship.
author_works = Table('author_works', Model.metadata,
    Column('author_id', Integer, ForeignKey('authors.id')),
    Column('work_id', Integer, ForeignKey('works.id'))
)


class Author(Model):
    """Represent an author of a work."""
    __tablename__ = 'authors'

    id = Column(Integer, primary_key=True)
    email = Column(String(255))
    first_name = Column(Unicode(255))
    last_name = Column(Unicode(255))
    affiliation = Column(Unicode(255))
    affiliation_address = Column(Unicode(255))
    occupation = Column(Unicode(128))
    phone = Column(String(255))

    user_id = Column(Integer, ForeignKey('users.id'))
    works = db.relationship('AuthorWorkAssociation', backref='author',
        order_by='AuthorWorkAssociation.author_id',
        cascade="all, delete-orphan")

    def __init__(self, first_name, last_name, email=None, affiliation=None,
                 affiliation_address=None, occupation=None, phone=None):
        self.email = email
        self.first_name = first_name
        self.last_name = last_name
        self.affiliation = affiliation
        self.affiliation_address = affiliation_address
        self.occupation = occupation
        self.phone = phone

    def __repr__(self):
        return '<Author %r>' % self.first_name


class Work(Model):
    """Represent a work registered by some user."""
    __tablename__ = 'works'

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(1024), unique=True)
    body = Column(Text)
    finished = Column(Boolean)
    approved = Column(Boolean)
    event_mode = Column(Integer)
    created_on = Column(DateTime)
    last_modified = Column(DateTime)

    user_id = Column(Integer, ForeignKey('users.id'))
    tag = db.relationship('WorkTag', cascade="all, delete, delete-orphan",
            order_by='WorkTag.id', uselist=False, backref='work')

    def __init__(self, title, body, approved=False, finished=False,
                 event_mode=None):
        self.title = title
        self.body = body
        self.finished = finished
        self.approved = approved
        self.event_mode = WORK_MODE.get(event_mode, WORK_MODE['POSTER'])
        self.created_on = datetime.datetime.now()
        self.last_modified = datetime.datetime.now()

    def __repr__(self):
        return '<Work %r>' % self.title


class AuthorWorkAssociation(Model):
    """Association between a work and an author."""
    __tablename__ = 'author_work_association'

    author_id = Column(Integer, ForeignKey('authors.id'), primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id'), primary_key=True)
    description = Column(Unicode(255))

    work = db.relationship('Work', backref=backref('author_assocs',
        cascade='all, delete-orphan'), order_by='Work.id')

    def __repr__(self):
        return '<AuthorWorkAssociation %r>' % self.description


# class AuthorTag(Model):
#     """Represent a tag to classify users."""
#     __tablename__ = 'author_tags'

#     id = Column(Integer, primary_key=True)
#     name = Column(Unicode(255))
#     author_id = Column(Integer, ForeignKey('authors.id'))


class WorkTag(Model):
    """Represents a tag to classify works."""
    __tablename__ = 'work_tags'

    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255))
    work_id = Column(Integer, ForeignKey('works.id'))


# class Poll(Model):
#     __tablename__ = 'poll'

#     id = Column(Integer, primary_key=True)
#     source = Column(Unicode(255))
#     user_id = Column(Integer, ForeignKey('users.id'))


class AuthenticationToken(Model):
    __tablename__ = 'auth_tokens'

    id = Column(Integer, primary_key=True)
    value = Column(String(1024), unique=True)
    expires = Column(DateTime)
    user_id = Column(Integer, ForeignKey('users.id'))


###########################################################
##              Utilities for response objects
###########################################################
def have_common_user(work, author):
    """Verify that two Work and Author objects are linked to the same
    User instance. The function assumes both objects are associated
    with some user."""
    return work.user is author.user


def stringify_datetime(date_object):
    return date_object.strftime('%d/%m/%Y %H:%M:%S')


def jsonify_user(user, include_password=True, include_works=False):
    data = {
        'id':               user.id,
        'email':            user.email,
        'password':         user.password,
        'name':             user.name,
        'lastAccess':       stringify_datetime(user.last_access),
        'role':             str(user.role),
        'gravatarUrl':      gravatar_url(user.email),
        'gravatarUrlSmall': gravatar_url(user.email, size=50)
    }
    if include_password is False:
        del data['password']
    if include_works is True:
        works_list = []
        for work in user.works:
            works_list.append(jsonify_work(work, user=False, authors=False))
        data['works'] = works_list
    return data


def jsonify_author(author, works=True):
    data = {
        'id':                   author.id,
        'email':                author.email,
        'lastName':             author.last_name,
        'firstName':            author.first_name,
        'affiliation':          author.affiliation,
        'affiliationAddress':   author.affiliation_address,
        'occupation':           author.occupation,
        'phone':                author.phone
    }
    data['url'] = url_for('author', author_id=author.id, _external=True)
    if works is True:
        data['associations'] = []
        for assoc in author.works:
            assoc_json = {}
            assoc_json['description'] = assoc.description
            work = assoc.work
            work_json = jsonify_work(work, tag=True, authors=False)
            assoc_json['work'] = work_json
            data['associations'].append(assoc_json)
    return data


def jsonify_work(work, user=True, tag=True, authors=True):
    data = {
        'id':               work.id,
        'title':            work.title,
        'body':             work.body,
        'approved':         work.approved,
        'finished':         work.finished,
        'eventMode':        work.event_mode,
        'createdOn':        stringify_datetime(work.created_on),
        'lastModified':     stringify_datetime(work.last_modified)
    }
    data['url'] = url_for('work', work_id=work.id, _external=True)
    if user is True:
        data['user'] = jsonify_user(work.user, include_password=False)
    if authors is True:
        data['associations'] = []
        for assoc in work.author_assocs:
            assoc_json = {}
            assoc_json['description'] = assoc.description
            author = assoc.author
            author_json = jsonify_author(author, works=False)
            assoc_json['author'] = author_json
            data['associations'].append(assoc_json)
    if tag:
        if work.tag is None:
            data['tag'], data['otherTag'] = None, None
        else:
            _tag, other_tag = decompose_tag_other(work.tag.name)
            data['tag'] = _tag
            data['otherTag'] = other_tag
#        data['tag'] = None if work.tag is None else work.tag.name
    return data


def jsonify_tag(tag):
    return {'name': tag.name}


class DatabaseHelper(object):
    """Class to insert sample data in database."""

    def __init__(self):
        """Helper to populate database with sample data"""
        from sampledata import USERS, AUTHORS, WORKS

        self.users = USERS
        self.works = WORKS
        self.authors = AUTHORS
        self.work_tags = WORK_TAGS
        self.author_tags = AUTHOR_TAGS

    def reset_database(self):
        # Delete previous tables and posible existent sessions.
        db.session.rollback()
        db.drop_all()
        db.create_all()

    def populate(self):
        self.add_users()
        self.add_works()
        self.add_authors()
        db.session.commit()

    def add_users(self):
        for data in self.users:
            email = data.get('email', None)
            password = data.get('password', None)
            name = data.get('name', None)
            role = data.get('role', None)
            active = data.get('active', False)
            _password = encrypt_password(password, pbkdf2_secure=True)
            user = User(email, _password, name=name, role=role, active=active)
            db.session.add(user)

    def add_works(self):
        for data in self.works:
            title = data.get('title', None)
            body = data.get('body', None)
            event_mode = data.get('eventMode', None)
            work = Work(title, body, event_mode=event_mode)
            db.session.add(work)

    def add_authors(self):
        for data in self.authors:
            first_name = data.get('firstName', None)
            last_name = data.get('lastName', None)
            email = data.get('email', None)
            affiliation = data.get('affiliation', None)
            affiliation_address = data.get('affiliationAddress', None)
            occupation = data.get('occupation', None)
            phone = data.get('phone', None)
            author = Author(first_name, last_name, email=email,
                            affiliation=affiliation,
                            affiliation_address=affiliation_address,
                            occupation=occupation, phone=phone)
            db.session.add(author)

    def associate(self):
        users = User.query.all()
        works = Work.query.all()
        authors = Author.query.all()

        user0, user1, user2 = users
        work0, work1, work2 = works
        author0, author1, author2 = authors

        with dbsession.no_autoflush:
            user0.works = [work0]
            user0.authors = [author0, author2]

            # works0 and works1 authors only can be user0 authors
            awa0 = AuthorWorkAssociation(description=u"Expositor")
            awa1 = AuthorWorkAssociation(description=u"Secundario")
            awa0.work = work0
            awa1.work = work0
            author0.works = [awa0]
            author2.works = [awa1]
            # work0.authors = user0.authors

            # Only one author and one work for user1
            user1.works = [work1]
            user1.authors = [author1]
            # Do not link authors yet...

            # No more authors, only one work
            user2.works = [work2]

        # Save test data
        dbsession.commit()


#############################################################
##                 Validation section
#############################################################
class UniqueEmailValidator(validators.Email):
    """"""
    messages = {
        'NOT_UNIQUE': 'A user with email %(email)s already exists.'
    }

    def _to_python(self, value, state):
        user = getattr(state, 'user', None)
        q = dbsession.query(User).filter(User.email == value)
        if not q.count():
            return value
        if user is None or user.email != value:
            raise Invalid(self.message('NOT_UNIQUE', state, email=value),
                  value, state)
        return value


class UniqueTitleValidator(validators.UnicodeString):
    """"""
    messages = {
        'NOT_UNIQUE': 'A work with title %(title)s already exists.'
    }

    def _to_python(self, value, state):
        work = getattr(state, 'work', None)
        q = dbsession.query(Work).filter(Work.title == value)
        if not q.count():
            return value
        if work is None or work.title != value:
            raise Invalid(self.message('NOT_UNIQUE', state, title=value),
                  value, state)
        return value


class AdminApproveValidator(validators.Bool):
    """"""

    messages = {
        'NOT_AUTHORIZED': 'You are not authorized to approve any work.'
    }

    def _to_python(self, value, state):
        user = getattr(state, 'user', None)
        if value is True:
            if user is None or not is_admin(user):
                raise Invalid(self.message('NOT_AUTHORIZED', state),
                    value, state)
        return value


class WorkBodyValidator(validators.UnicodeString):

    messages = {
        'MAX_SIZE': 'String has more words than allowed: %(nw)i > %(max)i.'
    }

    def _to_python(self, value, state):
        # Our way to count words.
        words = re.split(r'\s+', value)
        if len(words) > MAX_BODY_WORDS:
            raise Invalid(self.message('MAX_SIZE', state, nw=len(words),
                    max=MAX_BODY_WORDS), value, state)
        return validators.UnicodeString._to_python(self, value, state)


class RequireIfOther(FormValidator):

    # Field that potentially is required:
    required = None

    # If this field is present, then it is required:
    other = None

    __unpackargs__ = ('required',)

    def _to_python(self, value_dict, state):
        is_empty = self.field_is_empty
        if self.other == 'Otra' and is_empty(value_dict.get(self.required)):
            raise Invalid(
                _('You must give a value for %s') % self.required,
                value_dict, state,
                error_dict={self.required:
                    Invalid(self.message('empty', state),
                        value_dict.get(self.required), state)})
        return value_dict


class UserValidator(Schema):
    """Validator for author data"""

    name = validators.UnicodeString(not_empty=True)
    email = UniqueEmailValidator()
    password = validators.UnicodeString(min=8)
    confirm_password = validators.UnicodeString()
    chained_validators = [validators.FieldsMatch('password',
                                                  'confirm_password')]


class AuthorValidator(Schema):
    """"""

    first_name = validators.UnicodeString(not_empty=True)
    last_name = validators.UnicodeString(not_empty=True, max=MAX_BODY_WORDS)
    email = validators.Email()
    affiliation = validators.UnicodeString()
    affiliation_address = validators.UnicodeString()
    occupation = validators.UnicodeString()
    phone = validators.UnicodeString()


class WorkValidator(Schema):
    """"""

    title = UniqueTitleValidator(not_empty=True)
    body = WorkBodyValidator(not_empty=True)
    finished = validators.Bool()
    approved = AdminApproveValidator()
    event_mode = compound.Pipe(validators.Int(),
            validators.OneOf(list(WORK_MODE.itervalues())))
    tag = validators.OneOf(list(WORK_TAGS))
    other_tag = validators.UnicodeString(if_missing=None)
    chained_validators = [RequireIfOther('other_tag', other='tag')]


class ValidationState(object):
    pass


#############################################################
##                     REST resources
#############################################################
def authentication_required(f):
    """Decorator to force authentication"""
    @wraps(f)
    def decorated(*args, **kwargs):
        credentials = request.authorization
        if credentials is None:
            return make_response(json.jsonify({"message": "Not authorized"}),
                                 401)
        email = credentials.username
        password = credentials.password
        user = dbsession.query(User).filter(User.email == email).filter(
                User.password == password).first()
        if user is None:
            return USER_NOT_FOUND_ERROR, 404
        user.last_access = _now()
        dbsession.commit()
        g.current_user = user
        response = f(*args, **kwargs)
        return response
    return decorated


class UserResource(Resource):
    """"""
    method_decorators = [authentication_required]

    def get(self, user_id):
        logged_user = get_logged_user()
        if user_id == logged_user.id:
            user = logged_user
        else:
            if not is_admin(logged_user):
                return RESOURCE_FORBIDDEN, FORBIDDEN
            user = dbsession.query(User).filter(User.id == user_id).first()
            if user is None:
                return USER_NOT_FOUND_ERROR, NOT_FOUND

        if user_id != logged_user.id and not is_admin(logged_user):
            return RESOURCE_FORBIDDEN, FORBIDDEN
        user = dbsession.query(User).filter(User.id == user_id).first()
        if user is None:
            return USER_NOT_FOUND_ERROR, NOT_FOUND

        data = jsonify_user(user)
        return data, OK

    def put(self, user_id):
        logged_user = get_logged_user()
        if user_id == logged_user.id:
            user = logged_user
        else:
            if not is_admin(logged_user):
                return RESOURCE_FORBIDDEN, FORBIDDEN
            user = dbsession.query(User).filter(User.id == user_id).first()
            if user is None:
                return USER_NOT_FOUND_ERROR, NOT_FOUND

        data = request.get_json()
        name = data.get('name', None)
        email = data.get('email', None)
        new_password = data.get('newPassword', None)
        confirm_password = data.get('confirmPassword', None)
        state = ValidationState()
        state.user = logged_user

        if new_password is None and confirm_password is None:
            update_password = False
            new_password = confirm_password = '__FAKE_PASSWORD__'
        else:
            update_password = True

        try:
            v_data = UserValidator(allow_extra_fields=True).to_python(
                    dict(name=name, email=email, password=new_password,
                    confirm_password=confirm_password), state)
        except Invalid, e:
            errors = e.unpack_errors()
        else:
            user.name = v_data.get('name')

            with dbsession.no_autoflush:
                if update_password is True:
                    password = v_data.get('password')
                    user.password = encrypt_password(password,
                        pbkdf2_secure=True)
                user.email = v_data.get('email')

            dbsession.commit()
            errors = None

        data = jsonify_user(user)
        if user is logged_user:
            data[AUTH_TOKEN] = gen_auth_token(logged_user.email,
                logged_user.password)
        data['errorMessages'] = errors
        return data, OK

    def delete(self, user_id):
        logged_user = get_logged_user()
        if user_id == logged_user.id:
            user = logged_user
        else:
            if not is_admin(logged_user):
                return RESOURCE_FORBIDDEN, FORBIDDEN
            user = dbsession.query(User).filter(User.id == user_id).first()
            if user is None:
                return USER_NOT_FOUND_ERROR, NOT_FOUND

        dbsession.delete(user)
        dbsession.commit()
        return DELETE_SUCCESS, OK


class UsersResource(Resource):
    method_decorators = [authentication_required]

    def get(self):
        logged_user = get_logged_user()
        if not is_admin(logged_user):
            return RESOURCE_FORBIDDEN, FORBIDDEN

        with_works = request.args.get('withWorks', None)
        qusers = dbsession.query(User)
        if with_works:
            qusers = qusers.join(User.works).filter(Work.finished == True)
        users = qusers.all()

        users_list = []
        for user in users:
            user_json = jsonify_user(user, include_password=False,
                include_works=True)
            users_list.append(user_json)

        return {'users': users_list}, OK


class CurrentUserResource(Resource):
    """Resource to get and modify the information from a single user."""
    method_decorators = [authentication_required]

    def get(self):
        logged_user = get_logged_user()
        data = jsonify_user(logged_user)
        return data, OK

    def put(self):
        """"""
        logged_user = get_logged_user()
        data = request.get_json()
        name = data.get('name', None)
        email = data.get('email', None)
        new_password = data.get('newPassword', None)
        confirm_password = data.get('confirmPassword', None)
        state = ValidationState()
        state.user = logged_user

        if new_password is None and confirm_password is None:
            update_password = False
            new_password = confirm_password = '__FAKE_PASSWORD__'
        else:
            update_password = True

        try:
            v_data = UserValidator(allow_extra_fields=True).to_python(
                dict(name=name, email=email, password=new_password,
                confirm_password=confirm_password), state)
        except Invalid, e:
            errors = e.unpack_errors()
            status = BAD_REQUEST
        else:
            logged_user.name = v_data.get('name')
            with dbsession.no_autoflush:
                if update_password is True:
                    password = v_data.get('password')
                    logged_user.password = encrypt_password(password,
                        pbkdf2_secure=True)
                logged_user.email = v_data.get('email')
            dbsession.commit()
            status = OK
            errors = None

        data = jsonify_user(logged_user)
        data[AUTH_TOKEN] = gen_auth_token(logged_user.email,
            logged_user.password)
        data['errorMessages'] = errors
        return data, status

    def delete(self, user_id):
        """"""
        logged_user = get_logged_user()
        dbsession.delete(logged_user)
        dbsession.commit()
        return DELETE_SUCCESS, OK


class AuthorResource(Resource):
    """Resource to get and modify information from a single author."""
    method_decorators = [authentication_required]

    """Resource to manage operations over application users."""
    def get(self, author_id):
        """Retrieve the author with id equal to author_id"""
        user = get_logged_user()
        # Only append work data when this query param is True
        iw = request.args.get('iw', False)
        ret_works = True if iw == 'true' else False
        author = dbsession.query(Author).filter(Author.id == author_id).first()
        if author is None:
            return AUTHOR_NOT_FOUND_ERROR, NOT_FOUND
        if author.user_id != user.id and not is_admin(user):
            return RESOURCE_FORBIDDEN, FORBIDDEN

        data = jsonify_author(author, works=ret_works)
        return data, OK

    def put(self, author_id):
        """Update the author with id equal to author_id"""
        user = get_logged_user()
        author = dbsession.query(Author).filter(Author.id == author_id).first()
        if author is None:
            return AUTHOR_NOT_FOUND_ERROR, NOT_FOUND
        if author.user_id != user.id and not is_admin(user):
            return RESOURCE_FORBIDDEN, FORBIDDEN

        request_data = request.get_json()
        first_name = request_data.get('firstName', None)
        last_name = request_data.get('lastName', None)
        email = request_data.get('email', None)
        affiliation = request_data.get('affiliation')
        affiliation_address = request_data.get('affiliationAddress', None)
        occupation = request_data.get('occupation', None)
        phone = request_data.get('phone', None)

        try:
            unv_data = dict(first_name=first_name, last_name=last_name,
                email=email, affiliation=affiliation,
                affiliation_address=affiliation_address,
                occupation=occupation, phone=phone)
            v_data = AuthorValidator(allow_extra_fields=True).to_python(
                unv_data)
        except Invalid, e:
            errors = e.unpack_errors()
            status = BAD_REQUEST
        else:
            with dbsession.no_autoflush:
                author.first_name = v_data.get('first_name')
                author.last_name = v_data.get('last_name')
                author.email = v_data.get('email')
                author.affiliation = v_data.get('affiliation')
                author.affiliation_address = v_data.get('affiliation_address')
                author.occupation = v_data.get('occupation')
                author.phone = v_data.get('phone')
            dbsession.commit()
            errors = None
            status = OK

        data = jsonify_author(author)
        data[VAL_MSGS] = errors
        return data, status

    def delete(self, author_id):
        """Delete the author with id equal to author_id"""
        user = get_logged_user()
        author = dbsession.query(Author).filter(Author.id == author_id).first()
        if author is None:
            return AUTHOR_NOT_FOUND_ERROR, NOT_FOUND
        if author.user_id != user.id and not is_admin(user):
            return RESOURCE_FORBIDDEN, FORBIDDEN

        if len(author.works) > 0:
            status = BAD_REQUEST
            message = {'message': 'Can\'t delete author because is linked to '
                'an existing work.'}
        else:
            dbsession.delete(author)
            dbsession.commit()
            status = OK
            message = {'message': 'Author deleted'}
        return message, status


class AuthorsResource(Resource):
    """Resource to get all the authors registered by a user, and to add a
    new one.
    """
    method_decorators = [authentication_required]

    def get(self):
        logged_user = get_logged_user()
        # Only append work data when this query param is True
        iw = request.args.get('iw', False)
        ret_works = True if iw == 'true' else False

        from_user = request.args.get('fromUser', None)
        if from_user is None:
            user = logged_user
        else:
            user = dbsession.query(User).filter(User.id == from_user).first()
            if user is None and is_admin(logged_user):
                return {'message': 'query parameter fromUser references '
                    'to a non existen user'}, NOT_FOUND

            if (user is None or user.id != logged_user.id) and not \
                is_admin(logged_user):
                return {'message': 'query parameter fromUser references '
                    'to a forbidden resource'}, FORBIDDEN

        q = dbsession.query(Author)
        # XXX: Maybe we should return 403 status code if a user request all
        # data and is not an admin.
        if False:  # is_admin(logged_user):
            authors = q.order_by(Author.id).all()
        else:
            authors = q.filter(Author.user_id == user.id). \
                order_by(Author.id).all()

        authors_list = []
        for author in authors:
            authors_list.append(jsonify_author(author, works=ret_works))
        return {'authors': authors_list}, OK

    def post(self):
        """"""
        logged_user = get_logged_user()
        request_data = request.get_json()
        first_name = request_data.get('firstName', None)
        last_name = request_data.get('lastName', None)
        email = request_data.get('email', None)
        affiliation = request_data.get('affiliation')
        affiliation_address = request_data.get('affiliationAddress', None)
        occupation = request_data.get('occupation', None)
        phone = request_data.get('phone', None)

        try:
            unv_data = dict(first_name=first_name, last_name=last_name,
                email=email, affiliation=affiliation,
                affiliation_address=affiliation_address,
                occupation=occupation, phone=phone)
            v_data = AuthorValidator(allow_extra_fields=True).to_python(
                unv_data)
        except Invalid, e:
            errors = e.unpack_errors()
            status = BAD_REQUEST
            data = errors
        else:
            with dbsession.no_autoflush:
                first_name = v_data.get('first_name')
                last_name = v_data.get('last_name')
                email = v_data.get('email')
                affiliation = v_data.get('affiliation')
                affiliation_address = v_data.get('affiliation_address')
                occupation = v_data.get('occupation')
                phone = v_data.get('phone')
                author = Author(first_name, last_name, email=email,
                    affiliation=affiliation,
                    affiliation_address=affiliation_address,
                    occupation=occupation, phone=phone)
                author.user = logged_user
            dbsession.add(author)
            dbsession.commit()
            status = CREATED
            data = jsonify_author(author)

        return data, status


class WorkResource(Resource):
    """Resource to get and modify information from a single work."""
    method_decorators = [authentication_required]

    """Resource to manage operations over works registered by users."""
    def get(self, work_id):
        """GET verb"""
        logged_user = get_logged_user()
        # Only append author data when this query param is True
        ia = request.args.get('ia', True)
        ret_authors = True if ia == 'true' else False
        work = dbsession.query(Work).filter(Work.id == work_id).first()
        if work is None:
            return WORK_NOT_FOUND_ERROR, NOT_FOUND
        if work.user_id != logged_user.id and not is_admin(logged_user):
            return RESOURCE_FORBIDDEN, FORBIDDEN

        # Include user information
        data = jsonify_work(work, user=True, authors=ret_authors)
        return data, OK

    def put(self, work_id):
        """"""
        logged_user = get_logged_user()
        work = dbsession.query(Work).filter(Work.id == work_id).first()
        if work is None:
            return WORK_NOT_FOUND_ERROR, NOT_FOUND
        if work.user_id != logged_user.id and not is_admin(logged_user):
            return RESOURCE_FORBIDDEN, FORBIDDEN

        request_data = request.get_json()
        title = request_data.get('title', None)
        body = request_data.get('body', None)
        approved = request_data.get('approved', None)
        finished = request_data.get('finished', None)
        event_mode = request_data.get('eventMode', None)
        tag = request_data.get('tag', None)
        other_tag = request_data.get('otherTag', None)
        state = ValidationState()
        state.work = work
        state.user = logged_user

        try:
            v_data = WorkValidator(allow_extra_field=True).to_python(dict(
                title=title, body=body, approved=approved, finished=finished,
                event_mode=event_mode, tag=tag, other_tag=other_tag), state)
        except Invalid, e:
            errors = e.unpack_errors()
            status = BAD_REQUEST
        else:
            errors = None
            with dbsession.no_autoflush:
                work.title = v_data.get('title')
                work.body = v_data.get('body')
                work.finished = v_data.get('finished')
                work.event_mode = v_data.get('event_mode')
                work.last_modified = _now()
                work.approved = approved
                _tag = build_tag_other(v_data.get('tag'), v_data.get('other_tag'))
                tag = WorkTag(name=_tag)
                work.tag = tag
            dbsession.commit()
            status = OK

        data = jsonify_work(work)
        data[VAL_MSGS] = errors
        return data, status

    def delete(self, work_id):
        logged_user = get_logged_user()
        work = dbsession.query(Work).filter(Work.id == work_id).first()
        if work is None:
            return WORK_NOT_FOUND_ERROR, NOT_FOUND
        if work.user_id != logged_user.id and not is_admin(logged_user):
            return RESOURCE_FORBIDDEN, FORBIDDEN

        if work.finished is True:
            return {'message': 'Can\' delete a finished work'}, BAD_REQUEST

        dbsession.delete(work)
        dbsession.commit()
        return {'Mesage': 'deleted'}, OK


class WorksResource(Resource):
    """Resource to get all works registered, and to add a new one."""
    method_decorators = [authentication_required]

    def get(self):
        logged_user = get_logged_user()
        q = dbsession.query(Work)
        qargs = request.args
        # Only append work data when this query param is True
        inc_authors = qargs.get('authors', None)
        inc_all = qargs.get('all', None)
        inc_user = qargs.get('user', None)
        inc_finished = qargs.get('finished', None)
        inc_approved = qargs.get('approved', None)

        if inc_all and is_admin(logged_user):
            qworks = q.order_by(Work.last_modified)
        else:
            qworks = q.filter(Work.user_id == logged_user.id). \
                order_by(Work.last_modified)

        if inc_approved:
            works = qworks.filter(Work.approved == True).all()
        elif inc_finished:
            works = qworks.filter(Work.finished == True).all()
        else:
            works = qworks.all()

        works_list = []
        for work in works:
            work_json = jsonify_work(work, user=inc_user, authors=inc_authors)
            if inc_user:
                work_json['user'] = jsonify_user(work.user,
                    include_password=False)
            works_list.append(work_json)
        return {'works': works_list}, OK

    def post(self):
        logged_user = get_logged_user()
        request_data = request.get_json()
        title = request_data.get('title', None)
        body = request_data.get('body', None)
        approved = request_data.get('approved', None)
        finished = request_data.get('finished', None)
        event_mode = request_data.get('eventMode', None)
        tag = request_data.get('tag', None)
        other_tag = request_data.get('otherTag', None)

        try:
            v_data = WorkValidator(allow_extra_field=True).to_python(dict(
                title=title, body=body, approved=approved, finished=finished,
                event_mode=event_mode, tag=tag, other_tag=other_tag))
        except Invalid, e:
            errors = e.unpack_errors()
            data = {}
            status = BAD_REQUEST
        else:
            errors = None
            with dbsession.no_autoflush:
                title = v_data.get('title')
                body = v_data.get('body')
                approved = v_data.get('approved')
                finished = v_data.get('finished')
                event_mode = v_data.get('event_mode')
    #            _tag = v_data.get('tag', None)
                _tag = build_tag_other(v_data.get('tag'), v_data.get('other_tag'))
                tag = WorkTag(name=_tag)
                work = Work(title, body, approved=approved, finished=finished,
                            event_mode=event_mode)
                work.tag = tag
                work.user = logged_user
            dbsession.add(work)
            dbsession.commit()
            data = jsonify_work(work, authors=False)
            status = CREATED

        data[VAL_MSGS] = errors
        return data, status


class LinkAuthorWorkResource(Resource):
    """"""
    method_decorators = [authentication_required]

    def post(self, author_id, work_id):
        """Creates a link between works and authors of the same user."""
        logged_user = get_logged_user()
        work = dbsession.query(Work).filter(Work.id == work_id).first()
        author = dbsession.query(Author).filter(Author.id == author_id).first()
        if work is None:
            return WORK_NOT_FOUND_ERROR, NOT_FOUND
        if author is None:
            return USER_NOT_FOUND_ERROR, NOT_FOUND
        if work.user_id != logged_user.id and not is_admin(logged_user):
            return RESOURCE_FORBIDDEN, FORBIDDEN
        if author.user_id != logged_user.id and not is_admin(logged_user):
            return RESOURCE_FORBIDDEN, FORBIDDEN
        if not work.user is author.user:
            return {'message': "Resources belong to different users"}, \
                BAD_REQUEST
        if work.finished is True:
            return {'message': 'Work is finished'}, BAD_REQUEST

        data = request.get_json()
        description = data.get('description', None)

        try:
            with dbsession.no_autoflush:
                assoc = AuthorWorkAssociation(description=description)
                assoc.work = work
                # As simple as this
                author.works.append(assoc)
            dbsession.commit()
            data = dict(author_id=author_id, work_id=work_id,
            description=description)
            status = CREATED
        except FlushError:
            data = {'message': 'Association already exists'}
            status = BAD_REQUEST
        return data, status

    def delete(self, author_id, work_id):
        """Updates a link between works and authors of the same user."""
#        logged_user = get_logged_user()
        assoc = dbsession.query(AuthorWorkAssociation).filter(
            AuthorWorkAssociation.author_id == author_id,
            AuthorWorkAssociation.work_id == work_id).first()
        if assoc is None:
            return {'message': 'Association not found'}, NOT_FOUND
        if assoc.work.finished is True:
            return {'message': 'Can\'t disasociate a finished work'}, \
                BAD_REQUEST

        dbsession.delete(assoc)
        dbsession.commit()
        return {'Mesage': 'deleted'}, OK


class LoginResource(Resource):
    """Generate a token for the given credentials and return it to the
    client. This token must to be used in the Authorization header of
    every HTTP request. The generated token is simply a base64 encoding
    of username:password string, so Basic Authentication can be used.
    Requests to this resource are not necessary when encoding to base64
    is done in client side.
    """
    def post(self):
        """Returns the user data and authentication token for an already
        authenticated user, or when user credentials are valid.
        """
        credentials = request.authorization
        if not credentials is None:
            email = credentials.username
            password = credentials.password
        else:
            login_data = request.get_json()
            if login_data is None:
                return LOGIN_BAD_REQUEST, BAD_REQUEST
            email = login_data.get('email', None)
            password = login_data.get('password', None)

        user = dbsession.query(User).filter(User.email == email).first()
        if user is None:
            return USER_NOT_FOUND_ERROR, NOT_FOUND
        hashed = user.password
        if not check_password_hash(hashed, password):
            return {'message': 'passwords does not match'}, NOT_FOUND

        token_msg = gen_auth_token(email, hashed)
        data = {'authorizationToken': token_msg}
        return data, OK


class LogoutResource(Resource):
    """Delete a token. This resource is only useful in an environment that
    requires cookies to send used credentials with every request."""
    pass


class SignupResource(Resource):
    """Manages the subscription of a user to the"""

    def post(self):
        data = request.get_json()
        name = data.get('name', None)
        email = data.get('email', None)
        password = data.get('password', None)
        confirm_password = data.get('confirmPassword', None)
        response_data = {}

        try:
            UserValidator().to_python(dict(name=name, email=email,
                    password=password, confirm_password=confirm_password))
        except Invalid, e:
            errors = e.unpack_errors()
            response_data[VAL_MSGS] = errors
            return response_data, FORBIDDEN
        else:
            _password = encrypt_password(password, pbkdf2_secure=True)
            user = User(email, _password, name=name)
            dbsession.add(user)
            dbsession.commit()
            response_data = {}
            errors = None
            response_data[AUTH_TOKEN] = gen_auth_token(user.email,
                user.password)
            response_data[VAL_MSGS] = errors

        return response_data, OK


class ListTagResource(Resource):
    """"""
    def get(self):
        return {'allTags': list(WORK_TAGS)}, OK


class UniqueEmailResource(Resource):
    """"""

    def post(self):
        data = request.get_json()
        user_id = data.get('exclude', None)
        email = data.get('email', None)
        user = dbsession.query(User).filter(User.id == user_id).first()
        state = ValidationState()
        state.user = user
        try:
            UniqueEmailValidator().to_python(email, state)
        except Invalid, e:
            errors = e.unpack_errors()
        else:
            errors = None

        q = dbsession.query(User.email).filter(User.email == email)
        exists = False if q.count() == 0 or errors is None else True
        return {'exists': exists, 'errors': errors}, OK


class DataDownloader(Resource):
    """Serves an Excel 2007 file with all the users and works registered
    only to an administrator user.
    """

    def get(self, auth):
        try:
            email, password = base64.b64decode(auth).split(':', 1)
            user = dbsession.query(User).filter(User.email == email).\
                filter(User.password == password).first()
        except TypeError:
            user = None
        if user is None or not is_admin(user):
            return RESOURCE_FORBIDDEN, FORBIDDEN

        attachment_name = 'Lista de Usuarios y Trabajos.xlsx'
        fp = os.path.join(app.root_path, 'files', 'SystemData.xlsx')
        generate_data_book(fp)
        mime_type = 'application/vnd.openxmlformats-officedocument' \
            '.spreadsheetml.sheet'
        return send_file(fp, mime_type, as_attachment=True,
            attachment_filename=attachment_name)


@app.after_request
def add_header(response):
    """Add headers to force IE rendering to avoid caching GET requests
    to Flask-Restful resources.
    """
    response.headers['Pragma'] = 'no-cache'
    response.headers['Cache-Control'] = 'no-cache'
    return response


def generate_data_book(fp):
    """Create an Excel 2007 file with the data of all users and works in
    the database.
    """
    book = Workbook(fp, {'default_date_format': 'dd/mm/yy'})
    sheet_users = book.add_worksheet('Usuarios')
    sheet_works = book.add_worksheet('Trabajos')

    title_fmt = book.add_format({'bold': True, 'italic': True,
        'font_size': 30})
    subtitle_fmt = book.add_format({'bold': True, 'italic': True,
        'font_size': 18})
    col_title_fmt = book.add_format({'bold': True})
    id_fmt = book.add_format({'valign': 'top', 'bold': True})
    normal_fmt = book.add_format({'valign': 'top', 'text_wrap': True})
    wrap = book.add_format({'text_wrap': True})

    # Start in B2 cell
    lr, lc = 1, 1

    ## Sheet for users
    row, col = lr, lc
    sheet_users.set_column('B:B', 10, id_fmt)
    sheet_users.set_column('C:Z', 40, normal_fmt)

    # Write column titles
    sheet_users.write(row, col, u'Segundo Coloquio de Energía', title_fmt)
    sheet_users.write(row + 1, col, u'Lista de usuarios', subtitle_fmt)

    # Leave blank row
    row += 3
    sheet_users.write(row, col, u'ID', col_title_fmt)
    sheet_users.write(row, col + 1, u'NOMBRE DE USUARIO', col_title_fmt)
    sheet_users.write(row, col + 2, u'CORREO ELECTRÓNICO', col_title_fmt)
    sheet_users.write(row, col + 3, u'MODO DE PARTICIPACIÓN', col_title_fmt)

    # Leave blank row
    row += 2
    users = User.query.all()
    for user in users:
        sheet_users.write(row, col, user.id)
        sheet_users.write(row, col + 1, user.name)
        sheet_users.write(row, col + 2, user.email)
        if len(user.works) == 0:
            sheet_users.write(row, col + 3, u'Presencial')
        else:
            sheet_users.write(row, col + 3, u'Activo (trabajos registrados)')
        row += 1

    # Sheet for works
    row, col = lr, lc
    sheet_works.set_column('B:B', 10, id_fmt)
    sheet_works.set_column('C:C', 40, normal_fmt)
    sheet_works.set_column('D:D', 80, normal_fmt)
    sheet_works.set_column('E:F', 20, normal_fmt)
    sheet_works.set_column('F:F', 20, normal_fmt)
    sheet_works.set_column('G:G', 80, normal_fmt)
    sheet_works.set_column('H:I', 20, normal_fmt)
    sheet_works.set_column('J:J', 40, normal_fmt)
    sheet_works.set_column('K:Z', 10, normal_fmt)

    sheet_works.write(row, col, u'Segundo Coloquio de Energía', title_fmt)
    sheet_works.write(row + 1, col, u'Lista de trabajos', subtitle_fmt)

    # Write column titles
    row += 3
    sheet_works.write(row, col, u'ID', col_title_fmt)
    sheet_works.write(row, col + 1, u'TÍTULO', col_title_fmt)
    sheet_works.write(row, col + 2, u'CONTENIDO', col_title_fmt)
    sheet_works.write(row, col + 3, u'CLASIFICACIÓN', col_title_fmt)
    sheet_works.write(row, col + 4, u'MODALIDAD', col_title_fmt)
    sheet_works.write(row, col + 5, u'AUTORES', col_title_fmt)
    sheet_works.write(row, col + 6, u'ENVIADO A REVISIÓN', col_title_fmt)
    sheet_works.write(row, col + 7, u'APROBADO', col_title_fmt)
    sheet_works.write(row, col + 8, u'REGISTRADO POR', col_title_fmt)

    row += 2
    works = Work.query.all()
    for work in works:
        sheet_works.write(row, col, work.id)
        sheet_works.write(row, col + 1, work.title)
        sheet_works.write(row, col + 2, work.body, wrap)

        if work.tag:
            _tag, _extra = decompose_tag_other(work.tag.name or 'Invalida')
            tag = _tag + ('' if _extra is None else ' - {0}'.format(_extra))
            sheet_works.write(row, col + 3, tag)

        if work.event_mode:
            sheet_works.write(row, col + 4,
                stringify_work_mode(work.event_mode))

        authors = []
        for assoc in work.author_assocs:
            author = assoc.author
            description = assoc.description
            elems = [author.first_name, author.last_name, author.email,
                author.occupation, author.affiliation,
                author.affiliation_address, author.phone]
            fields = [fld for fld in elems if fld is not None]
            author = ', '.join(fields)
            if description:
                author += (' - (' + description + ')')
            authors.append(author)
        sheet_works.write(row, col + 5, u'\n\n'.join(authors))

        sheet_works.write(row, col + 6, u'Sí' if work.finished else 'No')
        sheet_works.write(row, col + 7, u'Sí' if work.approved else 'No')

        user = work.user.name + '\n'
        user += '(Email: ' + work.user.email + ')'
        sheet_works.write(row, col + 8, user)

        row += 2

    # Now save general properties
    book.set_properties({'title': u'Lista de usuarios y trabajos del Segundo' \
        u' Coloquio de Energía', 'author': u'Seminario de Física y Cómputo,' \
        u' Facultad de Ciencias, UNAM'})
    book.close()


######################################################################
##                        Utility functions
######################################################################
def get_logged_user():
    user = getattr(g, 'current_user', None)
    if user is None:
        raise RuntimeError('No previous authentication process executed.')
    return user


def is_admin(user):
    return user.role == ROLES['ADMINISTRATOR']


def setup_email_verification(user):
    pass


def gen_auth_token(email, password):
    return base64.b64encode(':'.join([email, password]))


def encrypt_password(password, pbkdf2_secure=True):
    return pbkdf2_secure_password(password)


def gravatar_url(email, size=120):
    email_hash = hashlib.md5(email).hexdigest()
    url = '/'.join(['https://secure.gravatar.com/avatar', email_hash])
    if isinstance(size, int):
        return url + '?s=' + str(size)
    return url


def build_tag_other(tag, value):
    if tag == 'Otra':
        return '$::$'.join([tag, value])
    return tag


def decompose_tag_other(value):
    if value.startswith('Otra'):
        return value.split('$::$')
    return value, None


def pbkdf2_secure_password(password):
    """Function"""
    return generate_password_hash(password, method='pbkdf2:sha512:2000',
                                  salt_length=20)


def stringify_work_mode(mode):
    if not mode in (1, 2, 3):
        return u'Error, ¿qué sucedió aquí?'
    if mode == 1:
        return u'Exposición (Oral)'
    if mode == 2:
        return u'Cartel'
    if mode == 3:
        return u'Prototipos'


def _now():
    return datetime.datetime.now()


####################################################
# #                 Routing
####################################################
@app.route('/')
def index():
    return json.jsonify(message='Application working properly...')

@app.route('/test-email')
def view_email():
    return render_template('email-base.html', title='Test Title',
        username='Usuario 1')


@app.route('/make_config/')
def make_config_file():
    """Test route to build configuration file"""
    key = generate_secret_key()
    scheme = 'http'
    body = render_template('config.tpl.html', secret_key=key, scheme=scheme)
    save_config_file(body)
    return 'Created File'


@app.route('/robots.txt')
@app.route('/favicon.ico')
def send_static_files():
    return send_from_directory(app.static_folder, request.path[1:])


@app.errorhandler(404)
def page_not_found(error):
    return 'Resource not found', 404


# # XXX: No trailig slash in URL, may cause a mess.
api.add_resource(LoginResource, '/login', '/acceder',
    endpoint='login')

api.add_resource(LogoutResource, '/logout', '/salir',
    endpoint='logout')

api.add_resource(SignupResource, '/signup', '/registrarse',
    endpoint='signup')

api.add_resource(UsersResource, '/users',
    '/usuarios', endpoint='users')

api.add_resource(CurrentUserResource, '/current-user',
    '/usuario-actual', endpoint='current_user')

api.add_resource(UserResource, '/users/<int:user_id>',
    '/usuario/<int:user_id>', endpoint='user')

api.add_resource(WorkResource, '/works/<int:work_id>',
    '/trabajos/<int:work_id>', endpoint='work')

api.add_resource(WorksResource, '/works', endpoint='works')

api.add_resource(AuthorsResource, '/authors', endpoint='authors')

api.add_resource(AuthorResource, '/authors/<int:author_id>',
    '/autores/<int:author_id>', endpoint='author')

api.add_resource(UniqueEmailResource, '/check-email',
    endpoint='check_email')

api.add_resource(ListTagResource, '/work-tags', endpoint='work-tags')

api.add_resource(LinkAuthorWorkResource, '/link-aw/<int:author_id>/'
    '<int:work_id>')

api.add_resource(DataDownloader, '/download-data/<string:auth>',
    endpoint='works-downloader')
