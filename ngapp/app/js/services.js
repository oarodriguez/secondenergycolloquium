(function(window, angular) {
/**
 *
 */
'use strict';

/* Services */

// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('RegApp.services', ['ngResource', 'ngCookies', 'RegApp.config'])
.factory('BackendAPI', ['$resource', '$cookies', 'BACKEND_API_PREFIX',
function($resource, $cookies, BACKEND_API_PREFIX){
    /**
     *
     */
    var prefix = BACKEND_API_PREFIX;

    return {
        currentUser : $resource(prefix + '/api/current-user', {}, {
            update : {
                method: 'PUT'
            }
        }),
        users : $resource(prefix + '/api/users/:id', {id: '@id'}, {
            update : {
                method: 'PUT'
            }
        }),
        authors : $resource(prefix + '/api/authors/:id', {id: '@id'}, {
            update : {
                method : 'PUT'
            }
        }),
        works : $resource(prefix + '/api/works/:id', {id: '@id'}, {
            update : {
                method : 'PUT'
            }
        }),
        login : $resource(prefix + '/api/login', {}, {}),
        logout : $resource(prefix + '/api/logout', {}, {}),
        signup : $resource(prefix + '/api/signup', {}, {}),
        checkEmail : $resource(prefix + '/api/check-email', {}, {}),
        workTags : $resource(prefix + '/api/work-tags', {}, {}),
        linkAuthorWork : $resource(prefix + '/api/link-aw/:aid/:wid', {
            aid: '@aid',
            wid: '@wid'
        }, {
            update: {
                method: 'PUT'
            }
        })
    };
}]).
factory('UserService', ['$http', '$location', '$cookies', 'BackendAPI', function($http, $location, $cookies, BackendAPI) {
    /**
     * Simple determines if a currentUser is currently logged in.
     */
    var currentUser = null,
        currentToken = null,
        authCookieName = '_colloquium_authtoken';

    function userResource() {
        return currentUser;
    }

    function getAuthToken() {
        return currentToken;
    }

    function setAuthorization(token) {
        currentToken = token;
        $cookies[authCookieName] = token;
        $http.defaults.headers.common.Authorization = 'Basic ' + token;
    }

    function removeAuthorization() {
        delete $cookies[authCookieName];
        delete $http.defaults.headers.common.Authorization;
    }

    function syncAuthorization() {
        var token = $cookies[authCookieName];

        if (!token) return;
        currentToken = token;
        $http.defaults.headers.common.Authorization = 'Basic ' + token;
    }

    function authenticated() {
        return currentUser || $cookies[authCookieName] || $http.defaults.headers.common.Authorization;
    }

    function isAdmin() {
        return currentUser && (currentUser.role == 1);
    }

    function deleteUser() {
        currentUser = null;
    }

    function dumpUserResource() {
        removeAuthorization();
        deleteUser();
    }

    function logout() {
        dumpUserResource();
        if (!!$cookies._signup_success_flash) {
            delete $cookies._signup_success_flash;
        };
        window.console.log('User logged out');
        // window.console.log(currentUser);
        $location.path('/').search({});
    }

    function denyAccess() {
        $location.path('/forbidden');
        $location.replace();
    }

    function gotoProfile(queryArgs) {
        $location.replace();
        if (queryArgs) {
            $location.search(queryArgs);
        };
        $location.path('/vision-general');
    }

    function setUpLoggedUser(successFn, failureFn) {
        syncAuthorization();
        currentUser = BackendAPI.currentUser.get({}, successFn, failureFn);
        window.console.log('New user resource created.');
        // window.console.log(currentUser);
    }

    return {
        userResource : userResource,
        deleteUser : deleteUser,
        getAuthToken: getAuthToken,
        syncAuthorization : syncAuthorization,
        setAuthorization : setAuthorization,
        removeAuthorization : removeAuthorization,
        authenticated : authenticated,
        isAdmin: isAdmin,
        dumpUserResource : dumpUserResource,
        denyAccess : denyAccess,
        gotoProfile : gotoProfile,
        logout : logout,
        setUpLoggedUser: setUpLoggedUser
    };

}]).value('version', '0.1');

//code
})(window, window.angular);
