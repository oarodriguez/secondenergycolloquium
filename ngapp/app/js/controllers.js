(function(window, angular, undefined) {
'use strict';

/**
 * Important: Code is by no way DRY.
 * TODO: Make it DRY.
 */

/* Controllers */
var ngModule = angular.module('RegApp.controllers', ['ng', 'ngCookies']);

function StatusController($rootScope, $scope, $routeParams, $http, $location, BackendAPI, UserService) {
    /**
     * Controller to get and distribute information of a given user. Usually
     * this controller is the first to perform actions over logged users,
     * especially when the page is reloaded. The controller creates a
     * Resource to manage the logged user, and in conjuntion with UserService
     * keeps the state between controllers.
     */

    $scope.userLogged = UserService.authenticated;
    $scope.logout = function() {
        $scope.user = null;
        UserService.logout();
    };

    // Listen this event to synchronize the scope when a user sign up.
    // In this case this controller does not load any Resource, so the
    // scope of this is out of sync after a user has successfuly registered.
    $rootScope.$on('loginSuccess', function() {
        window.console.log('Event triggered');
        $scope.user = UserService.userResource();
        $scope.isAdmin = UserService.isAdmin;
    });

    if (!UserService.authenticated()) {
        return;
    }

    // Setup user when page is fully reloaded.
    UserService.setUpLoggedUser(function(user) {
        // Success callback
    }, function(response) {
        // Fail callback
        // XXX: We do not handle BAD REQUESTS yet...
        switch (response.status)
        {
            case 403:
                UserService.denyAccess();
                break;
            case 404:
                $location.path('/notfound');
                $location.replace();
                break;
            default:
                $location.path('/server-error').search({
                    statusCode: response.status
                });
                $location.replace();
        }
    });
    $scope.user = UserService.userResource();
    $scope.isAdmin = UserService.isAdmin;

}

ngModule.controller('StatusController', ['$rootScope', '$scope', '$routeParams', '$http', '$location', 'BackendAPI', 'UserService', StatusController]);


/**
 * Controller to get and update information of a given user.
 */
function DashboardController($scope, $routeParams, $http, $cookies, $location, BackendAPI, UserService) {

    function loadWorks() {

        $scope._worksReady = false;

        var worksResource = BackendAPI.works.get({}, function(data) {
            // Success callback
            $scope.works = data.works;
            $scope._worksReady = true;

        }, function(response) {
            // Error callback
            $scope._worksReady = true;

            // XXX: 4XX errors look unnecessary.
            switch (response.status)
            {
                case 403:
                    UserService.denyAccess();
                    break;
                case 404:
                    $location.path('/notfound');
                    $location.replace();
                    break;
                default:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
            }

        });

    }

    function loadAuthors() {

        $scope._authorsReady = false;

        var authorsResource = BackendAPI.authors.get({iw: true}, function(data) {
            // Success callback
            $scope.authors = data.authors;
            $scope._authorsReady = true;

        }, function(response) {
            // Failure callback
            $scope._authorsReady = true;

            // XXX: 4XX errors look unnecessary.
            switch (response.status)
            {
                case 403:
                    UserService.denyAccess();
                    break;
                case 404:
                    $location.path('/notfound');
                    $location.replace();
                    break;
                default:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
            }

        });
    }

    function deleteWork(id) {
        BackendAPI.works.remove({id: id}, function() {
            loadWorks();
            loadAuthors();
            window.console.log('Trabajo eliminado');
        }, function(response) {
            // Error callback
            // XXX: We only catch server error messages.
            switch (response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
        });
    }

    function deleteAuthor(id) {
        BackendAPI.authors.remove({id: id}, function() {
            loadWorks();
            loadAuthors();
            window.console.log('Autor eliminado');
        }, function(response) {
            // Error callback
            // XXX: We only catch server error messages.
            switch (response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
        });
    }

    function dataReady() {
        return $scope._worksReady && $scope._authorsReady;
    }


    function hasSignupSuccessMessage() {
        return !!$cookies._signup_success_flash;
    }

    function hideSignupSuccessMessage() {
        delete $cookies._signup_success_flash;
    }

    if (!UserService.authenticated()) {
        UserService.denyAccess();
        return;
    }

    $scope.deleteWork = deleteWork;
    $scope.deleteAuthor = deleteAuthor;
    $scope.dataReady = dataReady;
    $scope.user = UserService.userResource();
    $scope.hasSignupSuccessMessage = hasSignupSuccessMessage;
    $scope.hideSignupSuccessMessage = hideSignupSuccessMessage;
    loadWorks();
    loadAuthors();

}
ngModule.controller('DashboardController',
    ['$scope', '$routeParams', '$http', '$cookies', '$location', 'BackendAPI', 'UserService',
    DashboardController]
);



 /**
  * Controller to get and update information of a given user.
  */
function UserController($scope, $routeParams, $http, $location, BackendAPI, UserService) {


    var userResource,
        loggedUserResource = $scope.loggedUser = UserService.userResource();

    /**
     *
     */
    function showEditForm() {
        $scope.currentView = 'EDIT';
    }

    function showDetails() {
        $scope.currentView = 'PREVIEW';
    }

    function hideMessage() {
        $scope.updateSuccess = false;
        $scope.updateFailure = false;
    }

    function updateUser() {
        userResource.$update(function(resource) {
            // Success
            window.console.log('Data updated.');
            $scope.updateSuccess = true;
            $scope.updateFailure = false;

            // Server only sends an authorization token if current user and
            // updated user are the same.
            if(resource.authorizationToken) {
                UserService.setAuthorization(resource.authorizationToken);
            }

        }, function(response) {
            // Failure
            $scope.updateSuccess = false;
            $scope.updateFailure = true;

            // Only unacceptable errors are 5XX errors.
            switch (response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
        });
    }

    function matchPasswords(form) {
        if (form.newPassword.$pristine && form.confirmPassword.$pristine) {
            return true;
        }
        else if ($scope.user.newPassword == $scope.user.confirmPassword) {
            return true;
        }
        return false;
    }

    function disableSetSubmit(form) {
        if (form.email.$valid && matchPasswords(form)) {
            $scope.disableSub = false;
            return false;
        }
        $scope.disableSub = true;
        return true;
    }


    $scope.displayDetails = true;
    $scope.disableSetSubmit = disableSetSubmit;
    $scope.matchPasswords = matchPasswords;
    $scope.updateUser = updateUser;
    $scope.showDetails = showDetails;
    $scope.showEditForm = showEditForm;
    $scope.hideMessage = hideMessage;
    $scope.currentView = 'PREVIEW';

     if (!UserService.authenticated()) {
        UserService.denyAccess();
        return;
    }

    // Authorization credentials need explicit synchronization
    UserService.syncAuthorization();

    if($routeParams.id == 'actual') {
        userResource = loggedUserResource;
        $scope.user = $scope.loggedUser;
    } else {
        $scope.user = userResource = BackendAPI.users.get({id: $routeParams.id}, function(user) {
            // $scope.user = user;
            if (user.id == loggedUserResource.id) {
                $location.path('/usuario/actual/perfil');
                $location.replace();
            };
        }, function(response) {

            switch (response.status)
            {
                case 403:
                    UserService.denyAccess();
                    break;
                case 404:
                    $location.path('/notfound');
                    $location.replace();
                    break;
                default:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }

        });
    }

}
ngModule.controller('UserController',
    ['$scope', '$routeParams', '$http', '$location', 'BackendAPI', 'UserService',
     UserController]
);


/**
 *
 */
ngModule.controller('AuthorController',
    ['$scope', '$location', '$routeParams', '$timeout', 'BackendAPI', 'UserService',
function($scope, $location, $routeParams, $timeout, BackendAPI, UserService) {

    function displayEditForm() {
        $scope.currentView = 'EDIT';
    }

    function displayAuthors() {
        $scope.currentView = 'AUTHORS';
    }

    function updateAuthor() {
        $scope.author.$update(function(resource) {
            // Success callback
            //$scope.work = resource;
            $scope.updateSuccess = true;
        }, function(response) {
            // Error callback
            $scope.updateError = true;

            // XXX: We only catch server error messages.
            switch (response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
            window.console.log('Error saving work');
        });
    }

    function hideSuccessMessage() {
        $scope.updateSuccess = false;
    }

    function hideErrorMessage() {
        $scope.updateError = false;
    }

    function createAuthor() {
        $scope.author.$save(function(resource) {
            window.console.log('Work created.');
            $scope.saveSuccess = true;

            $timeout(handleCreateRedirect, 1000);
        }, function(response) {
            // Error handler
            switch(response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
        });
    }

    function handleCreateRedirect() {

        // Redirect to corresponding work if we came from there.
        if($routeParams.workid) {
            $location.search({authorview: true});
            $location.path('/trabajos/' + $routeParams.workid);
            $location.replace();
            return;
        }

        // Otherwise go to author edit page.
        $location.path('/autores/' + $scope.author.id);
        $location.replace();

    }

    function propagateFormState(form) {
        $scope.formPristine = form.$pristine;
        if(form.$invalid) {
            $scope.validForm = false;
            return false;
        }
        $scope.validForm = true;
        return true;
    }


    if (!UserService.authenticated()) {
        UserService.denyAccess();
        return;
    }

    $scope.currentView = 'EDIT';
    $scope.displayEditForm = displayEditForm;
    $scope.displayAuthors = displayAuthors;
    $scope.updateAuthor = updateAuthor;
    $scope.createAuthor = createAuthor;
    $scope.propagateFormState = propagateFormState;
    $scope.hideSuccessMessage = hideSuccessMessage;
    $scope.hideErrorMessage = hideErrorMessage;

    // Authorization credentials need explicit synchronization
    UserService.syncAuthorization();

    if ($routeParams.id == 'crear') {
        $scope.author = new BackendAPI.authors();
        $scope._new = true;
        return;
    }

    $scope.author = BackendAPI.authors.get({
        id: $routeParams.id
    }, function() {

        $scope.authorReady = true;
        //NavbarService.setMessage('Hola');

    }, function(response) {
        // Failure callback
        switch (response.status)
        {
            case 403:
                UserService.denyAccess();
                break;
            case 404:
                $location.path('/notfound');
                $location.replace();
                break;
            default:
                $location.path('/server-error').search({
                    statusCode: response.status
                });
                $location.replace();
                break;
        }

    });

}]);


/**
 *
 */
ngModule.controller('WorkController', ['$scope', '$routeParams', '$location', 'BackendAPI', 'UserService',
function($scope, $routeParams, $location, BackendAPI, UserService) {

    var MAXBODYWORDS = 300;

    function displayEditForm() {
        $scope.currentView = 'EDIT';
    }

    function displayPreview() {
        $scope.currentView = 'PREVIEW';
    }

    function displayAuthors() {
        $scope.currentView = 'AUTHORS';
    }

    function countWords(text) {
        return text.match(/\S+/g).length;
    }

    function hideMessage() {
        $scope.updateSuccess = false;
    }

    function hideModalMessage() {
        $scope.updateSuccessModal = false;
    }

    function wordsAdvice() {
        var words = countBodyWords();
        if (words <= 250) {
            return 'GOOD';
        } else if (250 < words && words <= MAXBODYWORDS) {
            return 'WARNING';
        }
        else {
            return 'ERROR';
        }
    }

    function updateWork() {
        $scope.work.$update(function(resource) {
            //$scope.work = resource;
            $scope.updateSuccess = true;
        }, function(response) {
            // Error handler
            switch(response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }

            window.console.log('Error saving work');
        });
    }

    function finalizeWork(argument) {
        // Set the finished property to true.
        $scope.work.finished = true;
        $scope.work.$update(function(resource) {
            //$scope.work = resource;
            $scope.updateSuccessModal = true;
        }, function(response) {
            // Error handler
            switch(response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
            window.console.log('Error saving work');
        });
    }

    function approveWork(argument) {
        // Set the finished property to true.
        $scope.work.approved = true;
        $scope.work.$update(function(resource) {
            //$scope.work = resource;
            $scope.updateSuccess = true;
        }, function(response) {
            // Error handler
            switch(response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
            window.console.log('Error saving work');
        });
    }

    function disapproveWork(argument) {
        // Set the finished property to true.
        $scope.work.approved = false;
        $scope.work.$update(function(resource) {
            //$scope.work = resource;
            $scope.updateSuccess = true;
        }, function(response) {
            // Error handler
            switch(response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
            window.console.log('Error saving work');
        });
    }

    function createWork() {
        $scope.work.$save(function(resource) {
            window.console.log('Work created.');
            $location.path('/trabajos/' + $scope.work.id);
        }, function(response) {
            // Error handler
            switch(response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
            window.console.log('Error creating work');
        });
    }

    function createAuthor() {
        $location.path('/autores/crear').search({workid: $scope.work.id});
    }

    function countBodyWords() {
        // window.console.log($scope.work.body);
        if (!$scope.work.body) return -1;
        $scope.bodyWords = $scope.work.body.match(/\S+/g).length;
        return $scope.bodyWords;
    }

    function associationAuthor(assoc) {
        var author = assoc.author;
        var props = [author.firstName + ' ' + author.lastName, author.email,
        author.ocuppation, author.affiliation, author.affiliationAddress,
        author.phone]
        return _.reject(props, function(val) {
            if (!val) {
                return true;
            };
            return false;
        }).join(', ');

    }

    function getUserAuthors() {

        var authorsResource = BackendAPI.authors.get({fromUser: $scope.work.user.id}, function(data) {
            // Success callback
            $scope.allAuthors = data.authors;
            $scope.unlinkedAuthors = getUnlinkedAuthors();

        }, function(response) {
            // Error handler
            switch(response.status)
            {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
            // Failure callback
        });

    }

    function getUnlinkedAuthors() {
        var associations = $scope.work.associations;
        var unlinked = _.filter($scope.allAuthors, function(author) {
            var ids = _.map(associations, function(association) {
                return association.author.id;
            });
            return _.indexOf(ids, author.id) == -1 ? true : false;
        });

        $scope.associationsReady = true;
        return unlinked;
    }

    function linkAuthor(author) {
        var link = new BackendAPI.linkAuthorWork();
        link.aid = author.id;
        link.wid = $scope.work.id;
        // console.log(author);
        link.description = author.linkDescription;
        $scope.associationsReady = false;

        link.$save(function(resource) {
            // Success callback
            console.log('Link Stablished');
            $scope.work.$get({id: $routeParams.id, ia: true}, function(work){
                // Success callback
                $scope.unlinkedAuthors = getUnlinkedAuthors();
            }, function(response) {
                // Error callback
                // Set explicitly associations as ready.
                $scope.associationsReady = true;

                switch(response.status)
                {
                    case 0:
                    case 500:
                    case 502:
                    case 503:
                        $location.path('/server-error').search({
                            statusCode: response.status
                        });
                        $location.replace();
                        break;
                }

            });
        });
    }

    function unlinkAuthor(author) {
        var linkResource = new BackendAPI.linkAuthorWork();
        linkResource.aid = author.id;
        linkResource.wid = $scope.work.id;
        $scope.associationsReady = false;

        linkResource.$delete(function(resource) {
            // Success callback
            author.linkDescription = '';
            console.log('Link Removed');
            $scope.work.$get({id: $routeParams.id, ia: true}, function(work) {
                // Success callback
                $scope.unlinkedAuthors = getUnlinkedAuthors();
            }, function(response) {
                // Failure callback
                // Set explicitly associations as ready.
                $scope.associationsReady = true;

                switch(response.status)
                {
                    case 0:
                    case 500:
                    case 502:
                    case 503:
                        $location.path('/server-error').search({
                            statusCode: response.status
                        });
                        $location.replace();
                        break;
                }
            });
        });
    }

    function propagateFormState(form) {
        $scope.formPristine = form.$pristine;
        if(form.$invalid) {
            $scope.validForm = false;
            return false;
        } else if($scope.bodyWords > MAXBODYWORDS) {
            $scope.validForm = false;
            return false;
        }
        $scope.validForm = true;
        return true;
    }

    /**
     * The finish button must be disabled in the next cases:
     *
     * * Work is new.
     * * Work has no authors.
     * * Work has more words than allowed.
     * * When the form has been modified and is valid.
     *   -
     *
     * Otherwise the work can be submitted to revision.
     */
    function disableWorkSubmit() {
        if($scope._new) {
            return true;
        }
        else if(!$scope.work.associations || $scope.work.associations.length==0) {
            return true;
        }
        else if($scope.bodyWords > MAXBODYWORDS) {
            return true;
        }
        else if(!$scope.formPristine && !$scope.validForm) {
            return true;
        }
        else {
            return false;
        }
    }

    if (!UserService.authenticated()) {
        UserService.denyAccess();
        return;
    }

    // Authorization credentials need explicit synchronization
    UserService.syncAuthorization();

    $scope.maxBodyWords = MAXBODYWORDS;
    $scope.currentView = 'PREVIEW';
    $scope.bodyWords = null;
    $scope.displayDetails = true;
    $scope.displayPreview = displayPreview;
    $scope.displayEditForm = displayEditForm;
    $scope.displayAuthors = displayAuthors;
    $scope.updateWork = updateWork;
    $scope.createWork = createWork;
    $scope.finalizeWork = finalizeWork;
    $scope.approveWork = approveWork;
    $scope.disapproveWork = disapproveWork;
    $scope.hideMessage = hideMessage;
    $scope.hideModalMessage = hideModalMessage;
    $scope.linkAuthor = linkAuthor;
    $scope.unlinkAuthor = unlinkAuthor;
    $scope.wordsAdvice = wordsAdvice;
    $scope.propagateFormState = propagateFormState;
    $scope.disableWorkSubmit = disableWorkSubmit;
    $scope.associationAuthor = associationAuthor;
    $scope.createAuthor = createAuthor;

    $scope.latex = '\\( LaTeX \\)';
    $scope.dispEqExample = "$$ \\int_0^{\\infty} e^{-x^2} \\, dx = \\frac{\\sqrt{\\pi}}{2} $$";


    BackendAPI.workTags.get(function(resource) {
        $scope.allTags = resource.allTags;
    });


    if ($routeParams.id == 'crear') {

        $scope.work = new BackendAPI.works({ia: true});
        $scope._new = true;

    } else {

        $scope.work = BackendAPI.works.get({
            id: $routeParams.id,
            ia: true
        }, function(work) {
            // Success callback

            // XXX: This is not always neccesary
            $scope.work = work;
            getUserAuthors();

            if ($routeParams.authorview) {
                $scope.currentView = 'AUTHORS';
            }

            if ($routeParams.editview) {
                $scope.currentView = 'EDIT';
            }

            // The control form must be set as pristine explicitly.
            $scope.formPristine = true;

        }, function(response) {

            switch (response.status)
            {
                case 403:
                    UserService.denyAccess();
                    break;
                case 404:
                    $location.path('/notfound');
                    $location.replace();
                    break;
                default:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }

        });
    };

    $scope.userIsAdmin = UserService.isAdmin;

}]);


/**
 * Access controller.
 */
ngModule.controller('AccessController', ['$scope', '$location', '$cookies', 'BackendAPI', 'UserService',
function($scope, $location, $cookies, BackendAPI, UserService) {

    /**
     * Send the data to sign up a user.
     */
    function register() {
        var signupData = {
            name : $scope.signup.name,
            email : $scope.signup.email,
            password : $scope.signup.password,
            confirmPassword : $scope.signup.confirmPassword
        };

        BackendAPI.signup.save(signupData, function(data) {
            /**
             * Success callback after send signup request. The received data
             * is the authorization header.
             */
            // window.console.log(data);
            UserService.setAuthorization(data.authorizationToken);

            UserService.setUpLoggedUser(function(user) {
                // Success callback after receiveing user data.
                $scope.user = user;
                // window.console.log($scope.user);

                // Let's try with some events
                $scope.$emit('loginSuccess', {});

                // Set a temporal cookie to show a welcome message.
                $cookies._signup_success_flash = '__FLASH_COOKIE__';
                UserService.gotoProfile();

            }, function(response) {
                // Error
                switch(response.status)
                {
                    case 0:
                    case 500:
                    case 502:
                    case 503:
                        $location.path('/server-error').search({
                            statusCode: response.status
                        });
                        $location.replace();
                        break;
                }

                window.console.log('Could not get user data.');
            });

        }, function(response) {
            /**
             * Failure callback. Couldn't sign up
             */
            switch (response.status) {
                case 0:
                case 500:
                case 502:
                case 503:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
            window.console.log('Signup process failed.');
        });

    }

    /*
*     * Starts the login process.
     */
    function login() {
        var loginData = {
            email : $scope.loginEmail,
            password : $scope.loginPassword
        };

        BackendAPI.login.save(loginData, function(data) {

            // Success callback
            UserService.setAuthorization(data.authorizationToken);
            UserService.setUpLoggedUser(function(user) {
                // Sucess callback

                $scope.user = user;
                $scope.$emit('loginSuccess', {});
                // window.console.log($scope.user);
                UserService.gotoProfile();
            }, function(response) {
                // Error
                switch(response.status)
                {
                    case 0:
                    case 500:
                    case 502:
                    case 503:
                        $location.path('/server-error').search({
                            statusCode: response.status
                        });
                        $location.replace();
                        break;
                }

                window.console.log('Could not get user data.');
            });

            $scope.user = UserService.userResource();

        }, function(response) {
            switch (response.status) {
                case 404:
                    $scope.loginError = true;
                    break;
                default:
                    $location.path('/server-error').search({
                        statusCode: response.status
                    });
                    $location.replace();
                    break;
            }
            window.console.log('Login process failed.');
        });
    }

    function hideLoginMessage() {
        $scope.loginError = false;
    }

    function hideServerErrorMessage() {
        $scope.serverError = false;
    }

    function signupPasswordsMatch(form) {
        if (form.password.$pristine && form.confirmPassword.$pristine) {
            return true;
        }
        else if ($scope.signup.password == $scope.signup.confirmPassword) {
            return true;
        }
        return false;
    }

    function disableSignupSubmit(form) {
        if (form.displayName.$invalid || form.email.$invalid || form.password.$invalid) {
            return true;
        }
        if (!signupPasswordsMatch(form)) {
            return true;
        }
        return false;
    }

    $scope.sendRegistrationData = register;
    $scope.sendLoginData = login;
    $scope.signupPasswordsMatch = signupPasswordsMatch;
    $scope.disableSignupSubmit = disableSignupSubmit;
    $scope.hideLoginMessage = hideLoginMessage;
    $scope.hideServerErrorMessage = hideServerErrorMessage;
    $scope.loginError = false;

    if (UserService.authenticated()) {
        UserService.gotoProfile();
    }

}]);


function ServerErrorController($scope, $routeParams) {

    switch ($routeParams.statusCode)
    {
        case 500:
            $scope.statusCode = 500;
            $scope.statusDescription = 'Error interno de servidor';
            break;
        case 502:
            $scope.statusCode = 502;
            $scope.statusDescription = 'Puerta de enlace incorrecta';
            break;
        default:
            $scope.statusCode = 0;
            $scope.statusDescription = 'Error no definido';
    }

}
ngModule.controller('ServerErrorController', ['$scope', '$routeParams', ServerErrorController]);


//code
})(window, window.angular);
