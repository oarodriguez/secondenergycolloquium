(function(angular) {
    'use strict';

var deps = ['RegApp.filters', 'RegApp.services', 'RegApp.directives', 'RegApp.controllers', 'RegApp.extraControllers'];

// Declare app level module which depends on filters, and services
angular.module('RegApp', deps).config(['$routeProvider', '$locationProvider', '$httpProvider',
function($routeProvider, $locationProvider, $httpProvider) {

    //$httpProvider.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8';
    //$httpProvider.defaults.headers.put['Content-Type'] = 'application/json;charset=utf-8';

    // $locationProvider.html5Mode(false);
    $locationProvider.hashPrefix('!');

    $routeProvider.when('/usuarios', {
        templateUrl: 'partials/allusers.html',
        controller: 'UsersController'
    });

    $routeProvider.when('/trabajos', {
        templateUrl: 'partials/allworks.html',
        controller: 'WorksController'
    });

    $routeProvider.when('/vision-general', {
        templateUrl : 'partials/dashboard.html',
        controller : 'DashboardController'
    });

    $routeProvider.when('/usuario/:id/perfil', {
        templateUrl : 'partials/user.html',
        controller : 'UserController'
    });

    $routeProvider.when('/trabajos/:id', {
        templateUrl : 'partials/work.html',
        controller : 'WorkController'
    });

    $routeProvider.when('/autores/:id', {
        templateUrl : 'partials/author.html',
        controller : 'AuthorController'
    });

    $routeProvider.when('/acceso', {
        templateUrl : 'partials/access.html',
        controller : 'AccessController'
    });

    $routeProvider.when('/', {
        templateUrl : 'partials/main.html'
    });

    $routeProvider.when('/forbidden', {
        templateUrl : 'partials/error403.html'
    });

    $routeProvider.when('/notfound', {
        templateUrl : 'partials/error404.html'
    });

    $routeProvider.when('/server-error', {
        templateUrl: 'partials/server-error.html',
        controller: 'ServerErrorController'
    });

    $routeProvider.otherwise({
        redirectTo : '/notfound'
    });
}]);

})(angular);
