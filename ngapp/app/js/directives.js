(function(window, ng) {
    'use strict';

    /* Directives */

    var ngModule = ng.module('RegApp.directives', ['ng']);

    ngModule.directive('appVersion', ['version', function(version) {
        return function(scope, elm, attrs) {
            elm.text(version);
        };
    }]);

    ngModule.directive('ngUniqueEmail', ['BackendAPI', function(BackendAPI) {
        return {
            require: 'ngModel',
            link : function(scope, elem, attrs, ctrl) {
                elem.bind('blur', function(e) {
                    scope.$apply(function() {
                        var val = elem.val();
                        var id = attrs.ngUniqueEmail;
                        BackendAPI.checkEmail.save({email: val, exclude: id}, function(data) {
                            // Success callback
                            ctrl.$setValidity('exists', !data.exists);
                        });
                    });
                });
            }
        };

    }]);


})(window, window.angular);
