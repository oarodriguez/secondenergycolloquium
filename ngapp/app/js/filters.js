(function(window, angular) {
    'use strict';

    /* Filters */
    var ngModule = angular.module('RegApp.filters', []);

    ngModule.filter('interpolate', ['version', function(version) {
        return function(text) {
            return String(text).replace(/\%VERSION\%/mg, version);
        };
    }]);

    ngModule.filter('emptymessage', [function() {
        return function(input, message) {
            return input ? input : message;
        };
    }]);

    ngModule.filter('accesslevel', [function() {
        return function(text) {
            return parseInt(text, 10)==1 ? 'Administrador' : 'Usuario registrado';
        };
    }]);


    ngModule.filter('workmode', [function() {
        return function(text) {
            var mode = parseInt(text, 10);
            if(mode==1) {
                return 'Exposición (Oral)'
            }
            if(mode==2) {
                return 'Cartel';
            }
            if(mode==3) {
                return 'Prototipos'
            }
        };
    }]);

    ngModule.filter('jsmathify', [function() {
        return function(val) {
            // XXX. I consider this a quick but dirty hack...
            MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
            return val;
        };
    }]);



})(window, window.angular)
