(function  (windows, ng) {
    'use strict';

    var ngModule = ng.module('RegApp.extraControllers', ['ng', 'ngCookies', 'RegApp.config']);

    function WorksController($scope, $location, $routeParams, $cookies, UserService, BackendAPI, BACKEND_API_PREFIX) {

        function retrieveWorks() {

            var qargs = {};

            if ($scope.allWorks) {
                qargs['all'] = true;
                qargs['user'] = true;
            }

            if ($scope.finishedOnly) qargs['finished'] = true;
            if ($scope.approvedOnly) qargs['approved'] = true;

            BackendAPI.works.get(qargs, function(data) {
                // Success Callback

                var token = UserService.getAuthToken();
                $scope.dataReady = true;
                $scope.works = data.works;
                $scope.downloadHref = BACKEND_API_PREFIX + '/api/download-data/' + token;

            }, function(response) {
                // Failure callback
            });

        }

        function updateView() {
            var qargs,
                allWorks = $scope.allWorks,
                finished = $scope.finishedOnly,
                approved = $scope.approvedOnly;

            qargs = {};
            if (allWorks) qargs['allWorks'] = allWorks;
            if (finished) qargs['finished'] = finished;
            if (approved) qargs['approved'] = approved;

            $location.search(qargs);
            $scope.dataReady = false;

            // retrieveWorks();
        }

        function initScope () {
            $scope.allWorks = $routeParams.allWorks || false;
            $scope.finishedOnly = $routeParams.finished || false;
            $scope.approvedOnly = $routeParams.approved || false;
            $scope.user = UserService.userResource();
            $scope.updateView = updateView;
            $scope.isAdmin = UserService.isAdmin;
        }

        if (!UserService.authenticated()) {
            UserService.denyAccess();
        }

        initScope();
        retrieveWorks();

    }

    ngModule.controller('WorksController', ['$scope', '$location', '$routeParams', '$cookies',
        'UserService', 'BackendAPI', 'BACKEND_API_PREFIX', WorksController]);


    function UsersController($scope, $location, $routeParams, $cookies, $http, UserService, BackendAPI, BACKEND_API_PREFIX) {

        function retrieveUsers() {

            var qargs = {};

            if ($scope.worksOnly) qargs['withWorks'] = true;

            BackendAPI.users.get(qargs, function(responseBody) {
                // Success callback

                var token = UserService.getAuthToken();
                $scope.dataReady = true;
                $scope.users = responseBody.users;
                $scope.downloadHref = BACKEND_API_PREFIX + '/api/download-data/' + token;

            }, function(response) {
                // Failure callback
                if (response.status == 403) {
                    UserService.denyAccess();
                }

                if (response.status == 404) {
                    $location.path('/notfound');
                    $location.replace();
                };
            });
        }

        function updateView() {
            var qargs,
                worksOnly = $scope.worksOnly;

            qargs = {};
            if(worksOnly) qargs['worksOnly'] = true;

            $location.search(qargs);
            $scope.dataReady = false;
        }

        function initScope () {
            $scope.worksOnly = $routeParams.worksOnly || false;
            $scope.loggedUser = UserService.userResource();
            $scope.updateView = updateView;
            $scope.isAdmin = UserService.isAdmin;
        }

        if (!UserService.authenticated()) {
            UserService.denyAccess();
        }

        initScope();
        retrieveUsers();
    }

    ngModule.controller('UsersController', ['$scope', '$location', '$routeParams', '$cookies', '$http',
        'UserService', 'BackendAPI', 'BACKEND_API_PREFIX', UsersController]);

})(window, window.angular);
