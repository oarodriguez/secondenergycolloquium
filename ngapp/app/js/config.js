(function (window, angular) {
    'use strict';

    /**
     * The default configuration of the application. These values should
     * be those used in production code.
     */

    var ngModule = angular.module('RegApp.config', []);

    ngModule.constant('BACKEND_API_PREFIX', '');

})(window, window.angular)