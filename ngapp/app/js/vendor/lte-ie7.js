/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-home' : '&#xe000;',
			'icon-home-2' : '&#xe001;',
			'icon-home-3' : '&#xe002;',
			'icon-users' : '&#xe003;',
			'icon-print' : '&#xe004;',
			'icon-lock' : '&#xe005;',
			'icon-lock-2' : '&#xe006;',
			'icon-unlocked' : '&#xe007;',
			'icon-signup' : '&#xe008;',
			'icon-star' : '&#xe009;',
			'icon-close' : '&#xe00a;',
			'icon-checkmark' : '&#xe00b;',
			'icon-airplane' : '&#xe00c;',
			'icon-pencil' : '&#xe00d;',
			'icon-user' : '&#xe00e;',
			'icon-tags' : '&#xe00f;',
			'icon-tag' : '&#xe010;',
			'icon-envelop' : '&#xe011;',
			'icon-mobile' : '&#xe012;',
			'icon-tablet' : '&#xe013;',
			'icon-mobile-2' : '&#xe014;',
			'icon-laptop' : '&#xe015;',
			'icon-screen' : '&#xe016;',
			'icon-busy' : '&#xe017;',
			'icon-remove' : '&#xe018;',
			'icon-briefcase' : '&#xe019;',
			'icon-lightning' : '&#xe01a;',
			'icon-switch' : '&#xe01b;',
			'icon-power-cord' : '&#xe01c;',
			'icon-bookmark' : '&#xe01d;',
			'icon-bookmarks' : '&#xe01e;',
			'icon-info' : '&#xe01f;',
			'icon-info-2' : '&#xe020;',
			'icon-checkmark-2' : '&#xe021;',
			'icon-spam' : '&#xe022;',
			'icon-facebook' : '&#xe023;',
			'icon-twitter' : '&#xe024;',
			'icon-twitter-2' : '&#xe025;',
			'icon-facebook-2' : '&#xe026;',
			'icon-google-plus' : '&#xe027;',
			'icon-google-plus-2' : '&#xe028;',
			'icon-feed' : '&#xe029;',
			'icon-feed-2' : '&#xe02a;',
			'icon-chrome' : '&#xe02b;',
			'icon-firefox' : '&#xe02c;',
			'icon-IE' : '&#xe02d;',
			'icon-opera' : '&#xe02e;',
			'icon-safari' : '&#xe02f;',
			'icon-html5' : '&#xe030;',
			'icon-html5-2' : '&#xe031;',
			'icon-reply' : '&#xe032;',
			'icon-forward' : '&#xe033;',
			'icon-pencil-2' : '&#xe034;',
			'icon-paperplane' : '&#xe035;',
			'icon-mail' : '&#xe036;',
			'icon-mobile-3' : '&#xe037;',
			'icon-search' : '&#xe038;',
			'icon-eye' : '&#xe039;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};