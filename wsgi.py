# def application(environ, start_response):
#     """WSGI Application"""
#     start_response('200 OK', [('Content-type','text/html')])
#     yield """
#         <h1>Hello World!</h1>
#         <p>This is
#         <a href="http://en.wikipedia.org/wiki/Web_Server_Gateway_Interface">Python WSGI</a> application
#         powered <a href="http://www.helicontech.com/zoo/">Helicon Zoo</a>.</p>
#         """
#     yield '<h2>WSGI environment variables</h2>\n<pre><code>'
#     sorted_envs = environ.keys()[:]
#     sorted_envs.sort()
#     for k in sorted_envs:
#         yield '{0:<24}: {1}\n'.format(k, environ[k])
#     yield '</code></pre>'

from app import app as application

if __name__ == '__main__':
    application.run()

